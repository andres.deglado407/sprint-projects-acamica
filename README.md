# Sprint project 2 - Delilah Restó

A continuación se muestra todos los paso a pasos necesarios desde la instalación de librerías y tecnologías usadas, hasta la creación de un usuario en la API y empezar a hacer pedidos al restaurante.


## Librerías

Las tecnologías usadas en el desarrollo de este código fueron: 

 - Node JS
 - Express
 - Swagger
 - Bcrypt
 - Dotenv
 - Express - JWT
 - Helmet
 - Joi
 - Jsonwebtoken
 - Mariadb
 - Morgan
 - Redis
 - Sequelize

Como herramientas para desarrollo:
 - Chai
 - Chai Http
 - Mocha
 - Nodemon

 
 Una vez clonado el repositorio de github con el comando **npm install** se descarga todas las librerías mencionadas. 


## Base de datos: 
En esta versión del proyecto, se uso MariaDB para el uso de bases de datos. Por este motivo se envía unos comandos sql para ejecutar y crear todos los elementos de la base de datos, entre las tablas, insertar valores, etc. Es importante que Maria DB se esté ejecutando en la maquina en la cual se provara la API. En la maquína en la cual se desarrollo se usó el usuario root con contraseña 12345, corriendo en el puerto 3306.
También es importante que Redis se esté ejecutando en la maquina. En la máquina usada este corría en el puerto 6379.
## Uso: 
ejecutar el comando ubicando la terminal en la carpeta del repositorio: 
node src/index.js
 ## Link de la documentación
 [Documentación](http://localhost:3000/api-docs)



## Tipos de Usuarios

Para esta API se cuenta con dos tipos de usuarios, usuarios registrados y un usuario admin. Este último está fijo por el código y no se pueden reproducir, es decir que para acceder como admin hay que ingresar al perfil con las siguientes credenciales: 

 - username: useradmin email: adminuser@gmail.com userpassword: 12345

Dentro del código se encuentra otros usuarios preinscritos como manera de ejemplo.

## Pasos para seguir desde registro hasta la confirmación de pedidos

En caso de ser un nuevo usuario, este se tiene que primero registrar en la ruta de /register en este se muestran campos obligatorios como username, userpassword, name, email y teléfono, tal cual lo muestra el ejemplo.  El código verificará si el email y username no está en uso. Al finalizar se arrojará un mensaje con la confirmación del registro. 
Seguido a esto se ingresa por la ruta /login y también se ingresan las credenciales (email y userpassword) por el cuadro al lado derecho cercano de donde se muestra el servidor. En la ruta, se responderá que se hizo un login exitoso en caso de haber enviado las credenciales correctas y se generará el token de acceso en caso de que el usuario esté activo. 
En la ruta /user_info el usuario logueado podrá visualizar los datos que envió del registro y en la de /user_info/addresses podrá crear su agenda de direcciones. 
Seguido a esto en la ruta /products el usuario puede visualizar el menú ofrecido por el restaurante(productos activos), con su id, nombre, descripción y precio. Es importante que el usuario identifique el id del producto que desea, ya que este es importante para realizar la orden. 
En la ruta /order/create/view_payment_methods y /order/create/view_user_addresses podrá visualizar los métodos de pago y direcciones de su agenda. Para la selección es importante que tenga en cuenta el id de cada elemento. En order/create  envía ids y cantidades de los productos que desea agregar en el pedido, como también el método de pago y la dirección de envío. Como respuesta se devuelve la orden ya creada en la base de datos, con el id de la orden para hacer posibles ediciones o confirmar la orden. 
En caso de querer cambiar algo del pedido y la orden aún no ha sido confirmada, en /order/edit_products/:id o en order/edit_items/:id  el usuario podrá modificar los productos seleccionados o el método de pago y la dirección (items). En caso de modificar los productos seleccionados y/o cantidades tendrá que enviar el pedido completo de nuevo con las modificaciones. 
En la ruta /order/view/:id con el id de la orden podrá visualizar su orden y confirmarlo en a ruta order/confirm/:id, una vez confirmado no podrá realizar modificaciones. 
Por último el usuario puede visualizar las ordenes que ha hecho en la ruta de /order/history_user

## Funciones adicionales como usuario Admin

En caso de ingresar como usuario admin se cuenta con las funciones de usuario normal y adicionalmente se cuenta con funciones adicionales. En la sección de usuarios se puede visualizar todos los usuarios registrados y puede dar de baja a un usuario con el username del mismo. Con respecto a los productos se pueden crear, borrar, editar nombre o precio. 
Existe una ruta adicional /payment_methods donde el admin podrá visualizar los medios de pago antes de ir a las ordenes, crear más medios, modificarlos y borrarlos. 
En la sección de ordenes, podrá también ver el historial de todas las ordenes y con el id podrá modificar el estado de la orden. Para esto último primero se realiza un get en order/update_status para entender el código de los estados de orden, donde sencillamente se enumera los estados del 1 al 5. Para editar el estado de una orden se manda el código correspondiente y el id de la orden.

Las variables de entorno se encuentran en los archivos adjuntos con el nombre .env . 
## Contacto
andres.deglado407@gmail.com
