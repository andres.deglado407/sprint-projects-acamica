const swaggerOptions = {
    definition: 
    {
        "openapi": "3.0.0",
        "info": {
          "title": "Sprint Project 1 - Delilah Restó",
          "version": "0.0.1",
          "description": "A continuación se muestra la documentación de la API solicitada por el restuaruante Delilah Restó. En esta existen dos tipos de usuarios, el usuario normal y el usuario admin. El usuario normal podrá registrarse a la página y ambos deben ver la lista de pedidos para así conocer los productos ofrecidos. Seguido a esto se procede ir a la ruta de la orden, donde con el id del producto y la cantidad que desean del mismo crean su orden. Por último los usuarios pueden realizar modificaciones hasta que sea confirmado por el mismo. El usuario admin puede contar con acciones adicionales como crear, editar y borrar tanto productos como medios de pago. Espero que disfruten del programa.",
          "contact": {
            "email": "andres.deglado407@gmail.com"
          }
        },
        "paths": {
          "/registro": {
            "summary": "Crea usuarios",
            "description": "Crea usuarios con los campos que se requieren en la API",
            "post": {
              "tags": [
                "Usuarios"
              ],
              "summary": "Crea un usuario para el sistema",
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/UsuarioRegistrado"
                    }
                  }
                }
              },
              "responses": {
                "201": {
                  "description": "Crea un usuario en el array de usuarios"
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              },
              "security": [ ]
            }
          },
          "/login": {
            "summary": "Login a la API",
            "description": "Se solicita credenciales al usuario registrado para dar acceso a la API",
            "get": {
              "tags": [
                "Usuarios"
              ],
              "summary": "Crea un usuario para el sistema",
              "responses": {
                "200": {
                  "description": "El usuario ha realizado login con exito"
                },
                "401": {
                  "description": "credenciales no autorizadas"
                }
              }
            }
          },
          "/info_usuario": {
            "summary": "Muestra información del Usuario",
            "description": "Retorna la información generada en el registro, con el ID de registro",
            "get": {
              "tags": [
                "Usuarios"
              ],
              "summary": "información del usuario",
              "responses": {
                "200": {
                  "description": "información del usuario logueado",
                  "content": {
                    "application/json": {
                      "schema": {
                        "$ref": "#/components/schemas/UsuarioLogeado"
                      }
                    }
                  }
                },
                "401": {
                  "description": "credenciales no autorizadas"
                }
              }
            }
          },
          "/info_usuario/usuarios": {
            "summary": "Retorna todos los usuarios registrados",
            "description": "Retorna un array de todos los usuarios registrados, información generada solo al usuario admin",
            "get": {
              "tags": [
                "Usuarios"
              ],
              "summary": "información de usuarios",
              "responses": {
                "200": {
                  "description": "Usuarios",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/UsuarioLogeado"
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "credenciales no autorizadas"
                }
              }
            }
          },
          "/medios_de_pago": {
            "summary": "Sección de medios de pago",
            "description": "Retorna todos los medios de pago habilitados en la plataforma",
            "get": {
              "tags": [
                "Medios de pago"
              ],
              "summary": "Retorna todos los medios de pago al usuario admin",
              "responses": {
                "200": {
                  "description": "Devuelve todos los medios de pago al usuario admin",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/MediosDePago"
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              }
            }
          },
          "/medios_de_pago/agregar": {
            "summary": "Sección de medios de pago",
            "description": "Creación de medios de pago por el usuario admin",
            "post": {
              "tags": [
                "Medios de pago"
              ],
              "summary": "Creación de medio de pago por usuario admin",
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/MedioCreado"
                    }
                  }
                }
              },
              "responses": {
                "201": {
                  "description": "Se ha creado el medio de pago"
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              }
            }
          },
          "/medios_de_pago/editar": {
            "summary": "Sección de medios de pago",
            "description": "Creación de medios de pago por el usuario admin",
            "put": {
              "tags": [
                "Medios de pago"
              ],
              "summary": "Edición de medio de pago por usuario admin",
              "parameters": [
                {
                  "in": "query",
                  "name": "id",
                  "description": "id del medio de pago a editar",
                  "required": true,
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "id": 1
                  }
                },
                {
                  "in": "query",
                  "name": "edicion",
                  "description": "la edición que se desea hacer en el medio seleccionado",
                  "required": true,
                  "schema": {
                    "type": "string"
                  },
                  "example": {
                    "edicion": "Paypal"
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Se ha editado el medio de pago"
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              }
            }
          },
          "/medios_de_pago/borrar": {
            "summary": "Sección de medios de pago",
            "description": "Eliminación de medios de pago por el usuario admin",
            "delete": {
              "tags": [
                "Medios de pago"
              ],
              "summary": "Borrar medio de pago por usuario admin",
              "parameters": [
                {
                  "in": "query",
                  "name": "id",
                  "description": "id del medio de pago a borrar",
                  "required": true,
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "id": 1
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Se ha borrado el medio de pago"
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              }
            }
          },
          "/productos": {
            "summary": "Sección de Productos",
            "description": "Retorna todos los productos registrados a todos los usuarios",
            "get": {
              "tags": [
                "Productos"
              ],
              "summary": "Retorna todos los productos a todos los usuarios",
              "responses": {
                "200": {
                  "description": "Devuelve todos los medios de pago al usuario admin",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/Producto"
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Entradas no son valias"
                }
              }
            }
          },
          "/productos/agregar": {
            "summary": "Sección de Productos",
            "description": "Creación de productos por el usuario admin",
            "post": {
              "tags": [
                "Productos"
              ],
              "summary": "Creación de productos por el usuario admin",
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/ProductoCreado"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Se ha agregado el producto"
                },
                "400": {
                  "description": "Entradas no son valias"
                }
              }
            }
          },
          "/productos/cambiar_nombre": {
            "summary": "Sección de Productos",
            "description": "Cambio de nombre del producto por el usuario admin",
            "put": {
              "tags": [
                "Productos"
              ],
              "summary": "Cambio de nombre del producto por el usuario admin",
              "parameters": [
                {
                  "in": "query",
                  "name": "id",
                  "description": "id del producto a editar",
                  "required": true,
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "id": 1
                  }
                },
                {
                  "in": "query",
                  "name": "nombre",
                  "description": "la edición que se desea hacer al nombre del producto seleccionado",
                  "required": true,
                  "schema": {
                    "type": "string"
                  },
                  "example": {
                    "edicion": "Pizza mediana"
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Se ha cambiado el nombre del producto seleccionado"
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              }
            }
          },
          "/productos/cambiar_precio": {
            "summary": "Sección de Productos",
            "description": "Cambio de precio del producto por el usuario admin",
            "put": {
              "tags": [
                "Productos"
              ],
              "summary": "Cambio de precio del producto por el usuario admin",
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/ProductoPrecio"
                    }
                  }
                }
              },
              "parameters": [
                {
                  "in": "query",
                  "name": "id",
                  "description": "id del producto a editar",
                  "required": true,
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "id": 1
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Se ha cambiado el precio del producto seleccionado"
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              }
            }
          },
          "/productos/borrar": {
            "summary": "Sección de Productos",
            "description": "Eliminación de productos por el usuario admin",
            "delete": {
              "tags": [
                "Productos"
              ],
              "summary": "Borrar producto por usuario admin",
              "parameters": [
                {
                  "in": "query",
                  "name": "id",
                  "description": "id del produtco a borrar",
                  "required": true,
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "id": 1
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Se ha borrado el medio de pago"
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              }
            }
          },
          "/orden": {
            "summary": "Sección de Ordenes",
            "description": "Espacio donde el usuario logueado realiza su orden",
            "post": {
              "tags": [
                "Ordenes"
              ],
              "summary": "Usuario genera la orden",
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/PedidoCreado"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Retorna un objeto con el mensaje de orden creada y el id de la misma",
                  "content": {
                    "application/json": {
                      "schema": {
                        "$ref": "#/components/schemas/OrdenRespuesta"
                      }
                    }
                  }
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              }
            }
          },
          "/orden/medios_de_pago": {
            "summary": "Informa los medios de pago al usuario logueado",
            "description": "Informa un array de todos los medios de pago al usuario logueado, de manera que toma el id del medio de pago para declararlo en su orden",
            "get": {
              "tags": [
                "Ordenes"
              ],
              "summary": "Información de medios de pago",
              "responses": {
                "200": {
                  "description": "Retorna un array de todos los medios de pago al usuario logueado",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/MediosDePago"
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "credenciales no autorizadas"
                }
              }
            },
            "put": {
              "tags": [
                "Ordenes"
              ],
              "summary": "Asignación del medio de pago a la orden",
              "description": "El usuario define el medio de pago con el id del medio y el id de la orden",
              "parameters": [
                {
                  "in": "query",
                  "name": "idMedio",
                  "description": "id del medio de pago a definir en la orden",
                  "required": true,
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "idMedio": 1
                  }
                },
                {
                  "in": "query",
                  "name": "idOrden",
                  "description": "id de la orden de pago al cual se le asigna el medio de pago",
                  "required": true,
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "idOrden": 1
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Se ha definido el medio de pago en su orden",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/MediosDePago"
                        }
                      }
                    }
                  }
                },
                "402": {
                  "description": "se requiere la elección de un medio de pago"
                }
              },
              "security": [
                {
                  "basicAuth": []
                }
              ]
            }
          },
          "/orden/editar": {
            "summary": "Sección donde se edita el orden",
            "description": "En caso de que la orden no se encuentre confirmada, el usuario logeado aún podrá editar su orden con el id y podrá que enviar su orden completa, con la edición de nuevo.",
            "put": {
              "tags": [
                "Ordenes"
              ],
              "summary": "Edición de la orden por el usuario logeado",
              "description": "En caso de que la orden no se encuentre confirmada, el usuario logeado aún podrá editar su orden con el id y podrá que enviar su orden completa, con la edición de nuevo.",
              "parameters": [
                {
                  "in": "query",
                  "name": "id",
                  "description": "id de la orden aeditar",
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "id": 1
                  }
                }
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/PedidoCreado"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Se ha editado la orden seleccionada"
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              }
            }
          },
          "/orden/confirmar": {
            "summary": "El usuario logueado confirma su orden",
            "description": "En esta sección el usuario puede visualizar su orden y posterior confirmar, para esto el usuario require del id de la orden.",
            "get": {
              "tags": [
                "Ordenes"
              ],
              "summary": "Información de la orden por confirmar",
              "parameters": [
                {
                  "in": "query",
                  "name": "id",
                  "description": "id de la orden por confirmar",
                  "required": true,
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "id": 1
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Retorna la orden correspondiente al id",
                  "content": {
                    "application/json": {
                      "schema": {
                        "$ref": "#/components/schemas/Orden"
                      }
                    }
                  }
                },
                "400": {
                  "description": "Entradas no son válidas"
                }
              }
            },
            "put": {
              "tags": [
                "Ordenes"
              ],
              "summary": "Se confirma la orden del usuario por id",
              "description": "El usuario confirma su pedido después de verlo, a través del id de la orden",
              "parameters": [
                {
                  "in": "query",
                  "name": "id",
                  "description": "id de la orden por confirmar",
                  "required": true,
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "id": 1
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Su orden se ha confirmado"
                },
                "400": {
                  "description": "Entradas no son válidas"
                }
              },
              "security": [
                {
                  "basicAuth": []
                }
              ]
            }
          },
          "/orden/historial_ordenes_usuario": {
            "summary": "historial de ordenes del usuario",
            "description": "El usuario logueado puede ver el historial de sus ordenes",
            "get": {
              "tags": [
                "Ordenes"
              ],
              "summary": "Historial de ordenes del usuario",
              "responses": {
                "200": {
                  "description": "Retorna las ordenes correspondiente al usuario logueado",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/Orden"
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "/orden/historial_ordenes_usuarios": {
            "summary": "historial de ordenes de todos los usuarios",
            "description": "El usuario admin puede ver el historial de todas las ordenes",
            "get": {
              "tags": [
                "Ordenes"
              ],
              "summary": "Historial de ordenes",
              "responses": {
                "200": {
                  "description": "Retorna las ordenes",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/Orden"
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "/orden/actualizar_estado": {
            "summary": "Usuario admin actualiza el estado de una orden",
            "description": "El usuario admin logra actualizar el estado de la orden a través de su id y el estado que desea poner",
            "put": {
              "tags": [
                "Ordenes"
              ],
              "summary": "Usuario admin actualiza el estado de una orden",
              "description": "El usuario admin logra actualizar el estado de la orden a través de su id y el estado que desea poner",
              "parameters": [
                {
                  "in": "query",
                  "name": "id",
                  "description": "id de la orden a editar",
                  "schema": {
                    "type": "integer"
                  },
                  "example": {
                    "id": 1
                  }
                },
                {
                  "in": "query",
                  "name": "estado",
                  "description": "estado a poner de la orden",
                  "schema": {
                    "type": "string"
                  },
                  "example": {
                    "estado": "En preparación"
                  }
                }
              ],
              "responses": {
                "200": {
                  "description": "Se ha editado la orden seleccionada"
                },
                "400": {
                  "description": "Entradas no son validas"
                }
              }
            }
          }
        },
        "servers": [
          {
            "url": "http://localhost:3000",
            "description": "Local server"
          }
        ],
        "security": [
          {
              "basicAuth": []
          }
        ],

        "components": {
          "securitySchemes": {
            "basicAuth": {
              "type": "http",
              "scheme": "basic"
            }
          },
          "schemas": {
            "UsuarioRegistrado": {
              "type": "object",
              "required": [
                "username",
                "password",
                "nombre",
                "email",
                "tel",
                "dir"
              ],
              "properties": {
                "username": {
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "nombre": {
                  "type": "string"
                },
                "email": {
                  "type": "string"
                },
                "tel": {
                  "type": "string"
                },
                "dir": {
                  "type": "string"
                }
              },
              "example": {
                "username": "Andres",
                "password": "12345",
                "nombre": "Andrés Delgado Rivera",
                "email": "andel407@gmail.com",
                "tel": "3186432858",
                "dir": "cra 85c no 13b-29'"
              }
            },
            "UsuarioLogeado": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "username": {
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "nombre": {
                  "type": "string"
                },
                "email": {
                  "type": "string"
                },
                "tel": {
                  "type": "string"
                },
                "dir": {
                  "type": "string"
                },
                "isAdmin": {
                  "type": "boolean"
                }
              },
              "example": {
                "id": 1,
                "username": "Andres",
                "password": "12345",
                "nombre": "Andrés Delgado Rivera",
                "email": "andel407@gmail.com",
                "tel": "3186432858",
                "dir": "cra 85c no 13b-29'",
                "isAdmin": true
              }
            },
            "MediosDePago": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "medio": {
                  "type": "string"
                }
              },
              "example": {
                "id": 1,
                "medio": "Efectivo"
              }
            },
            "MedioCreado": {
              "type": "object",
              "required": [
                "medio"
              ],
              "properties": {
                "medio": {
                  "type": "object",
                  "properties":{
                    "medio": {
                      "type": "string" 
                    }
                  }
                }
              },
              "example": {
                "medio": {
                  "medio": "Nequi"
                }
              }
            },
            "Producto": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "nombre": {
                  "type": "string"
                },
                "descripcion": {
                  "type": "string"
                },
                "precio": {
                  "type": "number"
                }
              },
              "example": {
                "id": 1,
                "username": "Pizza personal",
                "descripcion": "Comida rápida sabor a carnes",
                "precio": 14500
              }
            },
            "ProductoCreado": {
              "type": "object",
              "properties": {
                "nombre": {
                  "type": "string"
                },
                "descripcion": {
                  "type": "string"
                },
                "precio": {
                  "type": "number"
                }
              },
              "required": [
                "nombre",
                "descripcion",
                "precio"
              ],
              "example": {
                "nombre": "Empanadas para compartir",
                "descripcion": "Seis empanadas de carne",
                "precio": 4500
              }
            },
            "ProductoPrecio": {
              "type": "object",
              "properties": {
                "precio": {
                  "type": "number"
              }
            },
              "required": [
                "precio"
              ],
              "example": {
                "precio": 16000
              }
            },
            "PedidoCreado": {
              "type": "object",
              "properties": {
                "direccion": {
                  "type": "string"
                },
                "pedido": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "integer"
                      },
                      "cantidad": {
                        "type": "number"
                      }
                    },
                    "required": [
                      "id",
                      "cantidad"
                    ]
                  }
                }
              },
              "required": [
                "pedido"
              ],
              "example": {
                "direccion": "cra 84a no 15-05",
                "pedido": [
                  {
                    "id": 1,
                    "cantidad": 5
                  }
                ]
              }
            },
            "OrdenRespuesta": {
              "type": "object",
              "properties": {
                "respuesta": {
                  "type": "string"
                },
                "id": {
                  "type": "integer"
                }
              },
              "example": {
                "respuesta": "Su orden se ha creado",
                "id": 1
              }
            },
            "Orden": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "username": {
                  "type": "string"
                },
                "nombre": {
                  "type": "string"
                },
                "tel": {
                  "type": "string"
                },
                "direccionEnvio": {
                  "type": "string"
                },
                "estado": {
                  "type": "string"
                },
                "productos": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "integer"
                      },
                      "nombre": {
                        "type": "string"
                      },
                      "descripcion": {
                        "type": "string"
                      },
                      "precio": {
                        "type": "number"
                      },
                      "cantidad": {
                        "type": "number"
                      }
                    }
                  }
                },
                "total": {
                  "type": "number"
                },
                "medioDePago": {
                  "type": "object",
                  "properties": {
                    "id": {
                      "type": "integer"
                    },
                    "medio": {
                      "type": "string"
                    }
                  }
                }
              },
              "example": {
                "direccion": "cra 84a no 15-05",
                "pedido": [
                  {
                    "id": 1,
                    "cantidad": 5
                  }
                ]
              }
            }
          }
        }
      },
    apis: [ ]
};

module.exports= swaggerOptions;