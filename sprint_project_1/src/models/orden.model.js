const { buscarProductoPorId } = require('./producto.model');

let ordenes = [
    {
        id: 1,
        username: "admin",
        nombre: "Admin de la página",
        tel: "3157492723",
        direccionEnvio: "cra 1 A av. 5-12",
        estado: "Confirmado",
        productos: [
            {
                id: 5,
                nombre: "Limonada cerezada",
                descripcion: "Bebida de limón y cereza de 16 onzas",
                precio: 7500,
                cantidad: 4
            },
            {
                id: 3,
                nombre: "Salchipapa tropical",
                descripcion: "Salchipapa con tocineta, maduro y queso para cuatro personas.",
                precio: 28000,
                cantidad: 1
            },
            {
                id: 2,
                nombre: "Sandwich cubano",
                descripcion: "Sandwich de jamón y queso, con lechuga y tomate.",
                precio: 7000,
                cantidad: 2
            }
        ],
        total: 72000,
        medioDePago: {
            id: 1,
            medio: "Efectivo"
        }
    },
    {
        id: 2,
        username: "Andres",
        nombre: "Andrés Delgado Rivera",
        tel: "3186432858",
        direccionEnvio: "Unicentro",
        estado: "Entregado",
        productos: [
            {
                id: 5,
                nombre: "Limonada cerezada",
                descripcion: "Bebida de limón y cereza de 16 onzas",
                precio: 7500,
                cantidad: 2
            },
            {
                id: 2,
                nombre: "Sandwich cubano",
                descripcion: "Sandwich de jamón y queso, con lechuga y tomate.",
                precio: 7000,
                cantidad: 2
            }
        ],
        total: 29000,
        medioDePago: {
            id: 3,
            medio: "Tarjeta crédito"
        }
    },
    {
        id: 3,
        username: "luis",
        nombre: "Luis H Delgado",
        tel: "3218523854",
        direccionEnvio: "cra 24 no 23-17",
        estado: "Entregado",
        productos: [
            {
                id: 1,
                nombre: "Pizza personal",
                descripcion: "Comida rápida sabor a carnes.",
                precio: 14500,
                cantidad: 2
            },
            {
                id: 4,
                nombre: "Cocacola 1.5 lts",
                descripcion: "Bebida azucarada",
                precio: 4500,
                cantidad: 1
            }
            
        ],
        total: 33500,
        medioDePago: {
            id: 2,
            medio: "Tarjeta débito"
        }
    }
];

const estadoDeOrden = ["Pendiente","Confirmado","En preparación","Enviado","Entregado"];

let contadorId = 4

const obtenerOrdenes = ()=> {
    return ordenes
}

const agregarOrden =(usuario,productos,direccion)=>{
    const orden = {
        id: contadorId++,
        username: usuario.username,
        nombre: usuario.nombre, 
        tel: usuario.tel,
        direccionEnvio: direccion ,
        estado: estadoDeOrden[0],
        productos: productos.productos,
        total:productos.total
    };
    ordenes.push(orden);
    return orden 
};

const agregarProductos = (pedido)=>{
    const productos = {
    productos:[]
    };
    pedido.forEach(producto=>{
        const productoconID = buscarProductoPorId(producto.id);
        productoconID.cantidad = producto.cantidad;
        productos.productos.push(productoconID);
    })
    return productos
};

const sumarTotal = (productos)=>{
    productos.total = 0;
    productos.productos.forEach(producto => {
        productos.total += producto.precio*producto.cantidad;    
    });
    return productos
};

const buscarOrdenId = (id)=>{
    const ordenResultado = ordenes.find(u=>u.id==id);
    return ordenResultado
};

const buscarOrdenesUsuario = (usuario)=>{
    const ordenesUsuario = ordenes.filter(u=>u.username==usuario);
    return ordenesUsuario
}

const actualizarEstado = (id,nuevoEstado)=>{
    const ordenEncontrada = buscarOrdenId(id);
    if (estadoDeOrden.find(u=>u==nuevoEstado)) {
        ordenEncontrada.estado = nuevoEstado;
        return ordenEncontrada
    } else {return ordenEncontrada}
}

const modificarPedido = (id,pedidoModificado)=>{
    const ordenEncontrada = buscarOrdenId(id);
    const carritoModificado = agregarProductos(pedidoModificado);
    sumarTotal(carritoModificado);
    ordenEncontrada.productos = carritoModificado.productos;
    ordenEncontrada.total = carritoModificado.total;
    return ordenEncontrada
}
const validarDireccion = (direccionEnvio, direccionRegisrtrada)=>{
    if (direccionEnvio) {
        return direccionEnvio
    } else {
        return direccionRegisrtrada
    }
};

const confirmarOrden = (id)=>{
    const ordenEncontrada = buscarOrdenId(id);
    ordenEncontrada.estado = estadoDeOrden[1];
    return ordenEncontrada
}
module.exports = { obtenerOrdenes, agregarOrden, agregarProductos, sumarTotal, buscarOrdenId, buscarOrdenesUsuario, actualizarEstado,modificarPedido,validarDireccion,confirmarOrden }


//pruebas 
