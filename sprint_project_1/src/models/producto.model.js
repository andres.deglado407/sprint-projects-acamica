const productos = [
    {
        id: 1,
        nombre: "Pizza personal",
        descripcion: "Comida rápida sabor a carnes.",
        precio: 14500
    }, 
    {
        id: 2,
        nombre: "Sandwich cubano",
        descripcion: "Sandwich de jamón y queso, con lechuga y tomate.",
        precio: 7000
    },
    {
        id: 3,
        nombre: "Salchipapa tropical",
        descripcion: "Salchipapa con tocineta, maduro y queso para cuatro personas.",
        precio: 28000
    },
    {
        id: 4,
        nombre: "Cocacola 1.5 lts",
        descripcion: "Bebida azucarada",
        precio: 4500
    },    {
        id: 5,
        nombre: "Limonada cerezada",
        descripcion: "Bebida de limón y cereza de 16 onzas",
        precio: 7500
    }
];

let contadorId = 6;

const obtenerProductos = ()=>{
    return productos;
};

const buscarProductoPorId = (idProducto)=>{
    const productoEncontrado = productos.find(u=> u.id==idProducto);
    return productoEncontrado
}

const agregarProducto = (productoNuevo)=>{
    productoNuevo.id = contadorId++;
    productos.push(productoNuevo);
    return productos
};

const borrarProducto = (idProducto) =>{
    const productoBorrar = buscarProductoPorId(idProducto);
    const indice = productos.indexOf(productoBorrar);
    productos.splice(indice,1);
    return productoBorrar
};

const cambiarNombreProd = (idProducto,nombreNuevo)=>{
    const productoACambiar = buscarProductoPorId(idProducto);
    productoACambiar.nombre = nombreNuevo;
    return productos
};

const cambiarPrecioProd = (idProducto,precioNuevo)=>{
    const productoACambiar = buscarProductoPorId(idProducto);
    productoACambiar.precio = precioNuevo;
    return productos
};


module.exports = { obtenerProductos,buscarProductoPorId, agregarProducto,borrarProducto,cambiarNombreProd, cambiarPrecioProd };

// console.log(buscarProductoPorId(7));

// if (buscarProductoPorId(7)) {
//     console.log("encontrado");
// } else {
//     console.log("No existe este elemento")
// }