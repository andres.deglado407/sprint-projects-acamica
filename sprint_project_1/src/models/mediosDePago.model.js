
const mediosDePago =[
    {
        id: 01,
        medio: "Efectivo"
    },
    {
        id: 02,
        medio: "Tarjeta débito"
    },
    {
        id: 03,
        medio: "Tarjeta crédito"
    }
];

let contadorId = 4;

const obtenerMedios = ()=>{
    return mediosDePago
}
const buscarMedioId = (idMedio)=>{
    return medioEncontrado = mediosDePago.find(u=>u.id==idMedio)
}
const agregarMedioDePago = (nuevoMedioDePago) =>{
    if (nuevoMedioDePago.medio && (typeof nuevoMedioDePago === 'object' && nuevoMedioDePago !== null) ) {
        nuevoMedioDePago.id = contadorId++;
        mediosDePago.push(nuevoMedioDePago);
        return true
    } else {
        return false
    }
}

const borrarMedioDePago = (idMedio) =>{
    const medioBorrar = mediosDePago.find(u=>u.id==idMedio);
    const indice = mediosDePago.indexOf(medioBorrar);
    mediosDePago.splice(indice,1);
    return medioBorrar
};

const editarMedioDePago = (idMedio,edicion)=>{
    const medioAEditar = buscarMedioId(idMedio);
    medioAEditar.medio= edicion;
    return mediosDePago
};

module.exports = {obtenerMedios,buscarMedioId,agregarMedioDePago,borrarMedioDePago,editarMedioDePago};