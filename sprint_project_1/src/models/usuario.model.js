const usuarios = [
    {
        id: 001,
        username: "Andres",
        password: "12345",
        nombre: "Andrés Delgado Rivera",
        email: "andel407@gmail.com",
        tel: '3186432858',
        dir: 'cra 85c no 13b-29',
        isAdmin: true
    },
    {
        id: 002,
        username: "admin",
        password: "12345",
        nombre: "Admin de la página",
        email: "admin123@gmail.com",
        tel: '3157492723',
        dir: 'cra 1 A av. 5-12',
        isAdmin: true
    },
    {
        id: 003,
        username: "luis",
        password: "12345",
        nombre: "Luis H Delgado",
        email: "luis@gmail.com",
        tel: '3218523854',
        dir: 'cra 24 no 23-17',
        isAdmin: false
    }
];

let contadorId = 4;

const obtenerUsuarios = ()=>{
    return usuarios;
};

const buscadorUsuario = (username)=>{
    const usuarioUsername = obtenerUsuarios().find(u=> u.username === username)
    return usuarioUsername
}

const agregarUsuarios = (usuarioNuevo)=>{
    usuarioNuevo.id = contadorId++;
    usuarios.push(usuarioNuevo);
    return usuarios
};

const borrarUsuario = (eliminarEmail) =>{
    const usuarioBorrar = usuarios.find(u=> u.email==eliminarEmail);
    const indice = usuarios.indexOf(usuarioBorrar);
    usuarios.splice(indice,1);
    return usuarios
};



module.exports = {obtenerUsuarios, buscadorUsuario, agregarUsuarios,borrarUsuario};

