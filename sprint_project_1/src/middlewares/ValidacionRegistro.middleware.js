const { obtenerUsuarios } = require('../models/usuario.model')

const validarEmail = (email)=>{
    if (obtenerUsuarios().find(u => u.email==email)) {
        return false;
    }else return true;   
};

const validarUsuario = (username)=>{
    if (obtenerUsuarios().find(u => u.username==username)) {
        return false;
    }else return true;   
};

const validarVacio = (usuario)=>{
    if ( (usuario.username.length && usuario.password.length && usuario.nombre.length && usuario.email.length && usuario.tel.length && usuario.dir.length) == 0  ) {
        return false
        } else return true
    
};

const validacionRegistro  = (req,res,next)=>{
    const { username, password, nombre, email, tel,dir } = req.body;
    const usuarioNuevo ={
        username: username,
        password: password,
        nombre: nombre,
        email: email, 
        tel: tel,
        dir: dir,
        isAdmin: false
    };
    if (validarVacio(usuarioNuevo)) {
        if (validarEmail(usuarioNuevo.email)) {
            if (validarUsuario(usuarioNuevo.username)) {
                next();
            }else res.status(400).json('El nombre de usuario ya se encuentra registrado, porfavor usar otro.');
        }else res.status(400).json('El correo ya se encuentra registrado, porfavor usar otro.');
    } else res.status(400).json('Llenar por completo los campos solicitados');   
};

module.exports = validacionRegistro; 
