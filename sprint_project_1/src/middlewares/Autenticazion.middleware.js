const { obtenerUsuarios } = require('../models/usuario.model')

const autenticacion = (usuario,contraseña)=> {
    const usuarioLogin = obtenerUsuarios().filter(u=> u.username === usuario && u.password === contraseña)
    if (usuarioLogin.length <= 0)return false;
    else return true;
}   

module.exports = autenticacion;