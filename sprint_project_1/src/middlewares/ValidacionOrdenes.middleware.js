const { buscarOrdenId } = require('../models/orden.model');
const { buscarProductoPorId } = require('../models/producto.model');
const { buscarMedioId } = require('../models/mediosDePago.model');

const validarVacioPedidos = (req,res,next)=>{
    const { pedido }= req.body;
    if ( pedido.length == 0  ) {
        res.status(400).json("No ha agregado ningún producto");
    } else next();
};

const validarProductosEncontrados = (req,res,next)=>{
    const { pedido }= req.body;
    const productosUndefined =[];
    pedido.forEach(producto => {
        if (buscarProductoPorId(producto.id)===undefined) {
            productosUndefined.push(producto.id); 
        }});
        if (productosUndefined.length==0) {
            next();
        } else {
            const respueta = {
                respond: "Los productos con los siguientes IDs no se encontraron dentro de los productos ofrecidos", 
                prod_no_disponibles: productosUndefined
            };
            res.status(400).json(respueta);
        } 
};

const validarMedioDePago = (req,res,next)=>{
    const { idMedio, idOrden } = req.query;
    const usuario = req.auth.user;
    const orden = buscarOrdenId(idOrden);
    console.log(orden);
    if (orden) {
        if (usuario==orden.username) {
            if (buscarMedioId(idMedio)) {
                next();           
            } else res.status(400).json("El id seleccionado no está dentro de la lista de medios de pago");
        } else  res.status(400).json("La orden no corresponde a su usuario de login");
    } else res.status(400).json("La orden seleccionada no existe");
};

const validarOrdenConUsuario  = (req,res,next)=>{
    const { id } = req.query;
    const orden = buscarOrdenId(id);
    const usuario = req.auth.user;
    if (orden) {
        if (usuario==orden.username) {
            next();
        } else res.status(400).json("La orden no corresponde a su usuario de login");
    } else res.status(400).json("La orden seleccionada no existe");
};

const validarExistenciaMediodePago  = (req,res,next)=>{
    const { id } = req.query;
    const orden = buscarOrdenId(id);
    if (orden.medioDePago) {
        next();
     } else res.status(402).json("La orden seleccionada no tiene asignado un medio de pago");
};

const validarEstadosParaEditar = (req,res,next)=>{
    const { id } = req.query;
    const usuario = req.auth.user;
    const orden = buscarOrdenId(id);
    if (usuario==orden.username) {
        if(orden.estado=="Pendiente"){
            next();
        }else res.status(400).json("Su orden ya se confirmó y por lo tanto no se puede modificar");
    } else res.status(400).json("La orden no corresponde a su usuario de login");

};


module.exports = { validarVacioPedidos, validarProductosEncontrados, validarMedioDePago, validarOrdenConUsuario,validarEstadosParaEditar,validarExistenciaMediodePago}; 
