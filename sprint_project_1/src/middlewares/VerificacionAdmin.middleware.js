const { buscadorUsuario } = require('../models/usuario.model');

const verificacionAdmin  = (req,res,next)=>{
    userLogged = buscadorUsuario(req.auth.user);
    if(userLogged.isAdmin) next();
    else {res.status(403).json('No tiene acceso a esta página')};
};

module.exports = verificacionAdmin;