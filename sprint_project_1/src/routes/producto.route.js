const express = require('express');
const router = express.Router();

const {  obtenerProductos, agregarProducto,borrarProducto,cambiarNombreProd, cambiarPrecioProd } = require('../models/producto.model');
const validacionAdmin = require('../middlewares/VerificacionAdmin.middleware');



router.get('/',(req,res)=>{
    res.status(200).json(obtenerProductos());
});

router.post('/agregar',validacionAdmin, (req,res)=>{
    const { nombre, descripcion, precio } = req.body;
    const productoNuevo ={
        nombre: nombre,
        descripcion: descripcion,
        precio: precio
    };
    agregarProducto(productoNuevo);
    res.status(201).json("Prdoucto agregado exitosamente");
});

router.put('/cambiar_nombre',validacionAdmin,(req,res)=>{
    const { id, nombre } = req.query;
    cambiarNombreProd(id,nombre)
    res.status(200).json('Se ha cambiado el nombre del producto');
});

router.put('/cambiar_precio',validacionAdmin,(req,res)=>{
    const { id } = req.query;
    const {precio} = req.body
    cambiarPrecioProd(id,precio)
    res.status(200).json('Se ha cambiado el precio del producto');
});

router.delete('/borrar', validacionAdmin,(req,res)=>{
    const { id } = req.query;
    borrarProducto(id);
    res.status(200).json('Producto borrado de manera exitosa');
});


module.exports = router;