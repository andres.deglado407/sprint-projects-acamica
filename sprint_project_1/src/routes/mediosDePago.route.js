const express = require('express');
const router = express.Router();

const { obtenerMedios,agregarMedioDePago,borrarMedioDePago,editarMedioDePago } = require('../models/mediosDePago.model');



router.get('/',(req,res)=>{
    res.status(200).json(obtenerMedios());
});

router.post('/agregar',(req,res)=>{
    const { medio } = req.body;
    const validación = agregarMedioDePago(medio); 
    if (validación){
        res.status(201).json("Se ha creado el medio de pago")
    }
    else res.status(400).json("El objeto ingresado no cumple con las caracteristicas para ser agregado como medio de pago");

});

router.put('/editar',(req,res)=>{
    const { id, edicion } = req.query;
    editarMedioDePago(id,edicion);
    res.status(200).json("Se editó el medio de pago con éxito")
});

router.delete('/borrar',(req,res)=>{
    const { id } = req.query;
    borrarMedioDePago(id);
    res.status(200).json("Se borró el medio de pago con éxito")
});

module.exports = router;
