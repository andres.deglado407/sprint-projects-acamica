const express = require('express');
const router = express.Router();
const basicAuth = require('express-basic-auth');
const autenticacion  = require('../middlewares/Autenticazion.middleware');



router.get("/",basicAuth({ authorizer: autenticacion}),(req,res)=>{
    res.status(200).json("Has realizado login con éxito");
});

module.exports= router;
