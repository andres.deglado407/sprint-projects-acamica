const express = require('express');
const router = express.Router();

const { obtenerUsuarios, buscadorUsuario } = require('../models/usuario.model');
const validacionAdmin = require('../middlewares/VerificacionAdmin.middleware');



router.get('/',(req,res)=>{
    const userLogged = buscadorUsuario(req.auth.user);
    res.status(200).json(userLogged);
});

router.get('/usuarios', validacionAdmin,(req,res)=>{
    res.status(200).json(obtenerUsuarios());
});

module.exports = router;