const express = require('express');
const router = express.Router();

const { buscadorUsuario } = require('../models/usuario.model');
const { obtenerMedios,buscarMedioId } = require('../models/mediosDePago.model');
const { obtenerOrdenes, agregarOrden, agregarProductos, sumarTotal, buscarOrdenId, buscarOrdenesUsuario, actualizarEstado, modificarPedido,validarDireccion,confirmarOrden } = require('../models/orden.model');


const validacionAdmin = require('../middlewares/VerificacionAdmin.middleware');
const { validarVacioPedidos, validarProductosEncontrados, validarMedioDePago, validarOrdenConUsuario,validarEstadosParaEditar, validarExistenciaMediodePago} = require('../middlewares/validacionOrdenes.middleware');

router.post('/',validarVacioPedidos, validarProductosEncontrados, (req,res)=>{
    const { direccion, pedido } = req.body;
    const productos = agregarProductos(pedido);
    const userLogged = buscadorUsuario(req.auth.user);
    const direccionRegistrada = userLogged.dir;
    const direccionEnvio = validarDireccion(direccion,direccionRegistrada);
    sumarTotal(productos);
    const ordenCreada = agregarOrden(userLogged,productos,direccionEnvio);
    const idOrden = ordenCreada.id;
    const respuesta = {
        respuesta: "Se ha creado la orden con el siguiente id",
        id: idOrden
    }
    res.status(200).json(respuesta);
}); 

router.get('/medios_de_pago',(req,res)=>{
    res.status(200).json(obtenerMedios());
})

router.put('/medios_de_pago', validarMedioDePago,(req,res)=>{
    const { idMedio, idOrden } = req.query;
    const medio = buscarMedioId(idMedio);
    const orden = buscarOrdenId(idOrden);
    orden.medioDePago = medio;
    res.status(200).json("Usted ha definido su medio de pago con éxito");
})

router.put('/editar',validarEstadosParaEditar, validarOrdenConUsuario, validarVacioPedidos, validarProductosEncontrados,(req,res)=>{
    const { id } = req.query;
    const { pedido } = req.body;
    const ordenModificada =modificarPedido(id,pedido);
    console.log(ordenModificada);
    res.status(200).json("Su orden se ha modificado");
});

router.get('/confirmar', validarOrdenConUsuario, (req,res)=>{
    const {id} = req.query;
    const orden = buscarOrdenId(id);
    res.status(200).json(orden); 
})

router.put('/confirmar',validarOrdenConUsuario, validarExistenciaMediodePago, (req,res)=>{
    const {id} =req.query;
    confirmarOrden(id);
    res.status(200).json("Su orden se ha confirmado");
})

router.get('/historial_ordenes_usuario',(req,res)=>{
    const usernameLogged = req.auth.user; 
    const ordenesDeUsuario = buscarOrdenesUsuario(usernameLogged);
    res.status(200).json(ordenesDeUsuario);
});

router.get('/historial_ordenes_usuarios',validacionAdmin,(req,res)=>{
    res.status(200).json(obtenerOrdenes());
}); 

router.put('/actualizar_estado',validacionAdmin,(req,res)=>{
    const { id, estado } = req.query;
    actualizarEstado(id,estado);
    res.status(200).json("se ha actualizado el estado");
});


module.exports = router;