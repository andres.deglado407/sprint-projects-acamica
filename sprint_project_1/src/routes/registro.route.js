const express = require('express');
const router = express.Router();
const validacionRegistro = require('../middlewares/validacionRegistro.middleware');

const { agregarUsuarios} = require('../models/usuario.model');



router.post("/",validacionRegistro,(req,res)=>{
    const { username, password, nombre, email, tel,dir } = req.body;
    const usuarioNuevo ={
        username: username,
        password: password,
        nombre: nombre,
        email: email, 
        tel: tel,
        dir: dir,
        isAdmin: false
    };
    agregarUsuarios(usuarioNuevo);

    res.status(201).json("usuario creado exitosamente");
});

module.exports = router;

