const express = require('express');
const basicAuth = require('express-basic-auth');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const autenticacion  = require('./middlewares/Autenticazion.middleware');
const validacionAdmin = require('./middlewares/VerificacionAdmin.middleware');

const registroRoute = require('./routes/registro.route');
const loginRoute = require('./routes/login.route')
const usuarioRoute = require('./routes/usuario.route');    
const mediosDePagoRoute = require('./routes/mediosDePago.route');
const productoRoute = require('./routes/producto.route');
const ordenRoute = require('./routes/orden.route');

const swaggerOptions = require('./utils/swaggerOptions');

const app = express();

const PORT = 3000;

app.use(express.json());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs',swaggerUI.serve, swaggerUI.setup(swaggerSpecs))

app.use('/registro',registroRoute);

app.use('/login',loginRoute);


app.use(basicAuth({ authorizer: autenticacion}));


app.use('/info_usuario',usuarioRoute);

app.use('/medios_de_pago',validacionAdmin,mediosDePagoRoute)

app.use('/productos',productoRoute);

app.use('/orden',ordenRoute);

app.listen(PORT, ()=>{
    console.log('Escuchando desde el puerto '+ PORT);
});

