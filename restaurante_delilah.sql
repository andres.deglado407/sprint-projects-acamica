-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.6.3-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando estructura para tabla restaurante_delilah.addresses
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(250) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isActive` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;

-- Volcando datos para la tabla restaurante_delilah.addresses: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` (`id`, `address`, `user_id`, `isActive`) VALUES
	(1, 'Cra 85c No 13B-29', 1, 0),
	(2, 'Cra 84a No 15-29', 1, 1);
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;

-- Volcando estructura para tabla restaurante_delilah.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `order_status` varchar(200) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `address_id` (`address_id`),
  KEY `payment_method_id` (`payment_method_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

-- Volcando datos para la tabla restaurante_delilah.orders: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `order_date`, `user_id`, `address_id`, `payment_method_id`, `order_status`, `total`) VALUES
	(1, '2021-09-19 22:30:57', 1, 2, 1, 'Entregado', 58000),
	(2, '2021-09-27 18:06:07', 1, 2, 1, 'Pendiente', 43000),
	(3, '2021-09-27 19:12:47', 1, 2, 1, 'Pendiente', 58000);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Volcando estructura para tabla restaurante_delilah.orders_products
CREATE TABLE IF NOT EXISTS `orders_products` (
  `orderId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) DEFAULT 1,
  KEY `orderId` (`orderId`),
  KEY `productId` (`productId`),
  CONSTRAINT `orders_products_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  CONSTRAINT `orders_products_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Volcando datos para la tabla restaurante_delilah.orders_products: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `orders_products` DISABLE KEYS */;
INSERT INTO `orders_products` (`orderId`, `productId`, `quantity`) VALUES
	(1, 1, 2),
	(1, 3, 2),
	(2, 1, 1),
	(2, 4, 1),
	(3, 1, 2),
	(3, 3, 2);
/*!40000 ALTER TABLE `orders_products` ENABLE KEYS */;

-- Volcando estructura para tabla restaurante_delilah.payment_methods
CREATE TABLE IF NOT EXISTS `payment_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_method` varchar(250) NOT NULL,
  `isActive` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `payment_method` (`payment_method`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;

-- Volcando datos para la tabla restaurante_delilah.payment_methods: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` (`id`, `payment_method`, `isActive`) VALUES
	(1, 'Efectivo', 1),
	(2, 'Tarjeta Credito', 0);
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;

-- Volcando estructura para tabla restaurante_delilah.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `price` float NOT NULL,
  `isActive` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product` (`product`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;

-- Volcando datos para la tabla restaurante_delilah.products: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `product`, `description`, `price`, `isActive`) VALUES
	(1, 'Pizza personal', 'Pizza para una persona de un solo ingrediente', 25000, 1),
	(2, 'pizza mediana', 'Pizza para 4 personas de un solo ingrediente', 30000, 0),
	(3, 'Cocacola grande', 'Gaseosa para dos personas ', 4000, 1),
	(4, 'Pizza pantalon', 'Pizza para una persona en diferente presentación y con relleno', 18000, 1);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Volcando estructura para tabla restaurante_delilah.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `userpassword` varchar(200) NOT NULL,
  `name_` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT 0,
  `isActive` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb3;

-- Volcando datos para la tabla restaurante_delilah.users: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `userpassword`, `name_`, `email`, `tel`, `isAdmin`, `isActive`) VALUES
	(1, 'useradmin', '$2b$10$pjpj9Sl7uHul78BDm02Kj.tOmpTtgqcOGH0Or3hjm1nOzb.UMoUIa', 'Admin User', 'adminuser@gmail.com', '3186432858', 1, 1),
	(2, 'user1', '$2b$10$DQ3NiKsC5qBmVhRVL/HzteJvOv9VhPHUBVNVW2E5.1638esTttGYC', 'User numero uno', 'user1@gmail.com', '3512851469', 0, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
