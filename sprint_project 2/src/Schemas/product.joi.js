const Joi = require('joi');

const ProductSchema = Joi.object({
    product: Joi.string()
        .required(),

    description: Joi.string()
        .required(),

    price: Joi.number()
        .required()
})
    .with('product', 'price');

module.exports = { ProductSchema };