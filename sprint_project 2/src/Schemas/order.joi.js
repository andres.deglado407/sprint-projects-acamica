const Joi = require('joi');


const OrderSchema = Joi.object({
    products: Joi.array().items(
        Joi.object({
            id: Joi.number()
                .required(),
            quantity: Joi.number(),
        })
    )
        .required(),

    idAddress: Joi.number()
        .required(),

    idPaymentMethod: Joi.number()
        .required()
});

const EditOrderByProductsSchema = Joi.object({
    products: Joi.array().items(
        Joi.object({
            id: Joi.number()
                .required(),
            quantity: Joi.number(),
        }))
  
});

const EditOrderByItemsSchema = Joi.object({
    idAddress: Joi.number()
        .required(),

    idPaymentMethod: Joi.number()
        .required()
})
module.exports = { OrderSchema, EditOrderByProductsSchema, EditOrderByItemsSchema };