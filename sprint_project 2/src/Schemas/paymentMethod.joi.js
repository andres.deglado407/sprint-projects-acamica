const Joi = require('joi');

const PaymentMethodSchema = Joi.object({
    payment_method: Joi.string()
        .min(3)
        .max(30)
        .required()
});

module.exports = { PaymentMethodSchema };