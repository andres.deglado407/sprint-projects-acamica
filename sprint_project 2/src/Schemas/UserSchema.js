const Joi = require('joi');

const UserSchema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    userpassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),

    repeatpassword: Joi.ref('userpassword'),

    name: Joi.string(),
    
    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
        .required(),

    tel: Joi.string()
        .min(7)
        .max(13)

})
    .with('username', 'email')
    .with('userpassword', 'repeatpassword');

const UserAddressSchema = Joi.object({
     address: Joi.string()
        .min(3)
        .max(50)
        .required()
});

const UserLoginSchema = Joi.object({
    email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),
    userpassword: Joi.string()
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
    .required()


})

module.exports = { UserSchema, UserAddressSchema, UserLoginSchema };
