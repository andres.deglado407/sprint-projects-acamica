const chai = require('chai');
const chaiHttp = require('chai-http');
const { deleteUserByUsername, createUser } = require('../models/user.model');

const app = require('../index');

chai.should();
chai.use(chaiHttp);

describe('User register',()=>{
    describe('POST /register successfully',()=>{
        it('Should return a status in 201 and a text User created successfully',(done)=>{
            const userTest = {
                username: 'userTest',
                userpassword: 'test123',
                repeatpassword: 'test123',
                name: 'User Test',
                email: 'test@email.com', 
                tel: '3708991452'
            };
            chai.request(app)
                .post('/register')
                .send(userTest)
                .end((err, response)=>{
                    response.should.have.status(201);
                    response.text.should.have.string('User created successfully');
                    done();
                });
        });
        after(async ()=>{
            await deleteUserByUsername('userTest');
        })
    });
    describe('POST /register unsuccessfully by sending bad request bodies',()=>{
        it('Should return a status in 400, because the username property does not satisfied the requirements, minimun 3',(done)=>{
            const userTest = {
                username: 'us',
                userpassword: 'test123',
                repeatpassword: 'test123',
                name: 'User Test',
                email: 'test@email.com', 
                tel: '3708991452'
            };
            chai.request(app)
                .post('/register')
                .send(userTest)
                .end((err, response)=>{
                    response.should.have.status(400);
                    response.text.should.have.string('\\"username\\" length must be at least 3 characters long');
                    done();
                });
        });
        it('Should return a status in 400, because the username property does not satisfied the requirements, maximum 30',(done)=>{
            const userTest = {
                username: 'us123456789012345678901234567890123',
                userpassword: 'test123',
                repeatpassword: 'test123',
                name: 'User Test',
                email: 'test@email.com', 
                tel: '3708991452'
            };
            chai.request(app)
                .post('/register')
                .send(userTest)
                .end((err, response)=>{
                    response.should.have.status(400);
                    response.text.should.have.string('\\"username\\" length must be less than or equal to 30 characters long');
                    done();
                });
        });
        it('Should return a status in 400, because the username property was not defined',(done)=>{
            const userTest = {
                userpassword: 'test123',
                repeatpassword: 'test123',
                name: 'User Test',
                email: 'test@email.com', 
                tel: '3708991452'
            };
            chai.request(app)
                .post('/register')
                .send(userTest)
                .end((err, response)=>{
                    response.should.have.status(400);
                    console.log(response.text)
                    response.text.should.have.string('\\"username\\" is required');
                    done();
                });
        });
        it('Should return a status in 400, because the userpassword property does not satisfied the requirements, minimun 3',(done)=>{
            const userTest = {
                username: 'userTest',
                userpassword: 'te',
                repeatpassword: 'te',
                name: 'User Test',
                email: 'test@email.com', 
                tel: '3708991452'
            };
            chai.request(app)
                .post('/register')
                .send(userTest)
                .end((err, response)=>{
                    response.should.have.status(400);
                    done();
                });
        });
        it('Should return a status in 400, because the userpassword property does not satisfied the requirements, maximum 30',(done)=>{
            const userTest = {
                username: 'userTest',
                userpassword: 'test123456789012345678901234567890123',
                repeatpassword: 'test123456789012345678901234567890123',
                name: 'User Test',
                email: 'test@email.com', 
                tel: '3708991452'
            };
            chai.request(app)
                .post('/register')
                .send(userTest)
                .end((err, response)=>{
                    response.should.have.status(400);
                    done();
                });
        });
        it('Should return a status in 400, because the userpassword property was not defined',(done)=>{
            const userTest = {
                username: 'userTest',
                repeatpassword: 'test123',
                name: 'User Test',
                email: 'test@email.com', 
                tel: '3708991452'
            };
            chai.request(app)
                .post('/register')
                .send(userTest)
                .end((err, response)=>{
                    response.should.have.status(400);
                    console.log(response.text)
                    response.text.should.have.string('\\"userpassword\\" is required');
                    done();
                });
        });
    });  
    describe('POST /register unsuccessfully due to an email or username already registered',()=>{
        beforeEach( async ()=>{{
            const userTest = {
                username: 'userTest',
                userpassword: 'test123',
                repeatpassword: 'test123',
                name: 'User Test',
                email: 'test@email.com', 
                tel: '3708991452'
            };
            await createUser(userTest)
        }});
        it('Should return a status in 400, because the email is already registered',(done)=>{
            const userTest = {
                username: 'userTest1',
                userpassword: 'test123',
                repeatpassword: 'test123',
                name: 'User Test',
                email: 'test@email.com', 
                tel: '3708991452'
            };
            chai.request(app)
                .post('/register')
                .send(userTest)
                .end((err, response)=>{
                    response.should.have.status(400);
                    response.text.should.have.string('This email is already used, please change it.');
                    done();
                });
        });
        it('Should return a status in 400, because the username is already registered',(done)=>{
            const userTest = {
                username: 'userTest',
                userpassword: 'test123',
                repeatpassword: 'test123',
                name: 'User Test',
                email: 'test1@email.com', 
                tel: '3708991452'
            };
            chai.request(app)
                .post('/register')
                .send(userTest)
                .end((err, response)=>{
                    response.should.have.status(400);
                    response.text.should.have.string('This username is already used, please change it.');
                    done();
                });
        });
        afterEach( async ()=>{{
            await deleteUserByUsername('userTest');
        }});
    });
});  