const config = require('./config/config');
const express = require('express');
const expressJWT = require('express-jwt');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const helmet = require("helmet");
const  morgan = require('morgan');

const { adminValidation } = require('./middlewares/AdminValidation.middleware');
const { isActiveValidation } = require('./middlewares/Authenticator.middleware');

const registerRoute = require('./routes/register.route');
const loginRoute = require('./routes/login.route')
const userRoute = require('./routes/user.route');    
const paymentMethodsRoute = require('./routes/paymentMethod.route');
const productRoute = require('./routes/product.route');
const orderRoute = require('./routes/order.route');


const swaggerOptions = require('./utils/swaggerOptions');
const swaggerSpecs = swaggerJsDoc(swaggerOptions);

const app = express();

app.use(express.json());
app.use(helmet());                  
app.use('/api-docs',swaggerUI.serve, swaggerUI.setup(swaggerSpecs))
app.use(morgan(':method :url :status :res[content-length] - :response-time ms')); 
app.use(expressJWT({
  secret: config.JWT_SIGNATURE,
  algorithms: ['HS256']
}).unless({
    path: ['/register','/login']
}));

app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('invalid token...');
  } 
});

app.use('/register',registerRoute);

app.use('/login',loginRoute);

app.use(isActiveValidation);

app.use('/user_info',userRoute);

app.use('/payment_methods',adminValidation,paymentMethodsRoute)

app.use('/products',productRoute);

app.use('/order',orderRoute);

app.listen(config.PORT, ()=>{
    console.log('Server listening on port '+ config.PORT);
});

module.exports = app;