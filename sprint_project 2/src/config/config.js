const dotenv = require('dotenv');

dotenv.config();

module.exports = { 
    PORT: process.env.PORT,
    DB_HOST: process.env.DB_HOST,
    DB_USER: process.env.DB_USER,
    DB_PASSWORD: process.env.DB_PASSWORD,
    DB_NAME: process.env.DB_NAME,
    SALT: process.env.SALT,
    JWT_SIGNATURE: process.env.JWT_SIGNATURE,
    REDIS_PORT: process.env.REDIS_PORT
}