const { Sequelize } = require('sequelize');

const config = require('./config');
const sequelize = new Sequelize(config.DB_NAME, config.DB_USER, config.DB_PASSWORD,{
    host: config.DB_HOST,
    dialect: 'mariadb',
    logging: false
});

(async ()=>{
    try {
        await sequelize.authenticate();
        console.log(`Succesfully connected to ${config.DB_NAME}`);
    } catch (error) {
        console.error('Connection failed: ',error);
    }
})();

module.exports = sequelize;
