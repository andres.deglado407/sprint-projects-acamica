const config = require('../config/config');
const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');

const { createUser, getUserByEmail, findUserByUsername, getUsers,
    getAddressesFromUser, createAddressUser, deleteAddressFromUser, deactivateUserByUsername  } = require('../models/user.model');

const registerController = async(req,res)=>{
    const { username, userpassword, name, email, tel } = req.body;
    const newUser ={
        username: username,
        userpassword: bcrypt.hashSync(userpassword,parseInt(config.SALT)),
        name: name,
        email: email, 
        tel: tel,
        isAdmin: false,
        isActive: true
    };
    await createUser(newUser);
    res.status(201).json("User created successfully");
};
const loginController = async(req,res)=>{
    const { email, userpassword } = req.body
   const userLogin = await getUserByEmail(email);
    if (userLogin) {
        const matchingResult = bcrypt.compareSync(userpassword, userLogin.userpassword);
        if (matchingResult) {
            if (userLogin.isActive) {
                const token = jsonwebtoken.sign({
                    id: userLogin.id,
                    username: userLogin.username,
                    name_: userLogin.name_,
                    email: userLogin.email
                },(config.JWT_SIGNATURE));
                const message = {
                    message: 'Your token is:',
                    token: token
                };
                res.status(200).json(message);
            } else res.status(401).json('Unauthorized');
        } else res.status(401).json('Unauthorized');
   } else res.status(404).json("There is no registred user with that email. Please register first.");   
   
};
const userInfoController = async(req,res)=>{
    const { username, name_, email, tel } =  await findUserByUsername(req.user.username);
    const userToShow = {
        username,
        name_,
        email,
        tel
    }
    res.status(200).json(userToShow);
};
const getAllUsersController =  async(req,res)=>{
    const users = await getUsers();
    res.status(200).json(users);
};
const getAddressesFromUserController =  async(req,res)=>{
    const userLogged =  await findUserByUsername(req.user.username);
    const userAddresses = await getAddressesFromUser(userLogged);
    if (userAddresses[0]) {
        res.status(200).json(userAddresses);
    } else {
        res.status(400).json('Please enter first an address.');
    }
};
const addingAddressController = async(req,res)=>{
    const { address } = req.body; 
    const userLogged =  await findUserByUsername(req.user.username);
    const addressCreated = await createAddressUser(userLogged,address);
    res.status(200).json(`You have entered successfully your address, the id to input in your order is ${addressCreated[0]}.`);
};
const deletingAddressController =  async(req,res)=>{
    const { id } = req.params;
    const userLogged =  await findUserByUsername(req.user.username);
    await deleteAddressFromUser(id, userLogged);
    res.status(200).json('You have deleted the address successfully.');
};
const inactiveUserController =  async(req,res)=>{
    const { username } = req.params;
    const userLogged =  await findUserByUsername(username);
    if (userLogged) {
        await deactivateUserByUsername(username);
        res.status(200).json(`You have deactivated the user ${username}, he can not login without permission.`);
    } else  res.status(404).json(`The username entered was not found.`)
};

module.exports = { registerController, loginController, userInfoController, getAllUsersController,
     getAddressesFromUserController, addingAddressController, deletingAddressController,inactiveUserController };
 