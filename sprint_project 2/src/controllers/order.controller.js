const { findUserByUsername, getAddressesFromUser } = require('../models/user.model');
const { getPaymentMethods } = require('../models/paymentMethod.model');
const {  creatingOrderByProducts, sumTotalOfProducts, addOrderToDB,
getOrderByIdFromDB,editingOrderByProductsInDB, editingOrderByItemsInDB, editStatusByAdmin,
confirmOrder, getOrdersFromUser, getAllOrders, getOrderStatus  } = require('../models/order.model');

const getAddressesForOrderController =  async(req,res)=>{
    const userLogged =  await findUserByUsername(req.user.username);
    const userAddresses = await getAddressesFromUser(userLogged);
    if (userAddresses.lenght === 0) {
        res.status(404).json('Please enter an address first.');
        
    } else {
        const respond = {
            message: "Your addresses are:",
            userAddresses: userAddresses
        };
        res.status(200).json(respond);
    }
};
const getPaymentMethodsForOrderController = async(req,res)=>{
    const paymentMethods = await getPaymentMethods();
    res.status(200).json(paymentMethods);
};
const createOrderController = async(req,res)=>{
    const { products, idAddress, idPaymentMethod } = req.body;
    const userLogged =  await findUserByUsername(req.user.username);
    const order = await creatingOrderByProducts(userLogged, products);
    order.idAddress= idAddress;
    order.idPaymentMethod = idPaymentMethod;
    order.total = sumTotalOfProducts(order);    
    const idOrder = await addOrderToDB(order);
    const orderInDB = await getOrderByIdFromDB(idOrder);
    const respond = {
        message: "The order is now created with the following items:",
        order: orderInDB
    }
    res.json(respond);
};
const viewOrderController = async(req,res)=>{
    const { id } = req.params;
    const orderInDB = await getOrderByIdFromDB(id);
    const respond = {
        message: "Your order is:",
        order: orderInDB
    };
    res.status(200).json(respond);
};
const confirmOrderController =  async(req,res)=>{
    const { id } = req.params;
    await confirmOrder(id);
    res.status(200).json("Your order has been confirmed");
};
const editOrderByProductsController =  async(req,res)=>{
    const { id } = req.params;
    const { products } = req.body;
    await editingOrderByProductsInDB(id, products);
    const orderEdited = await getOrderByIdFromDB(id);
    const respond = {
        message: "Your order has been edited. Check out the changes below.",
        order: orderEdited
    };
    res.status(200).json(respond);
};
const editOrderByItemsController = async(req,res)=>{
    const { id } = req.params;
    const {  idAddress, idPaymentMethod } = req.body;
    await editingOrderByItemsInDB(id, idAddress, idPaymentMethod);
    const orderEdited = await getOrderByIdFromDB(id);
    const respond = {
        message: "Your order has been edited. Check out the changes below.",
        order: orderEdited
    };
    res.status(200).json(respond);;
};
const getUpdateStatusController = (req,res)=>{
    const orderStatus = getOrderStatus();

    const respond = {
        message: "Select the update status by sending the number and ID",
        option_1: `1 ${orderStatus[0]}`,
        option_2: `2 ${orderStatus[1]}`,
        option_3: `3 ${orderStatus[2]}`,
        option_4: `4 ${orderStatus[3]}`,
        option_5: `5 ${orderStatus[4]}`
    }
    res.status(200).json(respond);
};
const updateOrderByAdminController = async(req,res)=>{
    const { id } = req.params;
    const { status } = req.query;
    const orderToChange = await getOrderByIdFromDB(id);
    if (orderToChange) {
        await editStatusByAdmin(id, status);
        res.status(200).json("The order selected was updated");
    } else {
        res.status(404).json("The order selected was not found in the DB");
    }
};
const getUserHistoryOrdersController = async(req,res)=>{
    const ordersInDB = await getOrdersFromUser(req.user.username);
    if (ordersInDB.length === 0) {
        res.status(400).json("Your user does not have any order, please create one.");
    } else res.status(200).json(ordersInDB);
};
const getHistoryOrdersController = async(req,res)=>{
    const ordersInDB = await getAllOrders();
    res.status(200).json(ordersInDB);
};

module.exports = { getAddressesForOrderController, getPaymentMethodsForOrderController, createOrderController, viewOrderController, confirmOrderController,
 editOrderByProductsController, editOrderByItemsController,getUpdateStatusController, updateOrderByAdminController, getUserHistoryOrdersController, getHistoryOrdersController };