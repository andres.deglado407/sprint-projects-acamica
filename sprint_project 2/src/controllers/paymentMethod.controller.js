const { getPaymentMethodsForAdmin, createPaymentMethod, editPaymentMehtod, 
    deletePaymentMethod, getPaymentMethodById } = require('../models/paymentMethod.model');

const getAllMethodsForAdminController = async(req,res)=>{
    const paymentMethods = await getPaymentMethodsForAdmin();
    const message = {
        message: "Bellow you can see all registered payment methods.",
        payment_methods: paymentMethods
    };
    res.status(200).json(message);
};
const addingPaymentMethodController = async(req,res)=>{
    const { payment_method } = req.body;
    const addedMethod = await createPaymentMethod(payment_method); 
    if (addedMethod){
        res.status(200).json(`The payment method ${payment_method} has been created with the id: ${addedMethod[0]}`)
    }
    else res.status(400).json("The payment method was not created, check the item entered.");
};
const editingPaymentMethodController = async(req,res)=>{
    const { id } = req.params;
    const { payment_method } = req.body;
    const editedMethod = await editPaymentMehtod(id, payment_method);
    if (editedMethod[1]==! 0) {
        res.status(200).json("The method you chose was edited.");
    } else res.status(400).json("The method you chose was not edited, please check the input entered.");
};
const deletingPaymentMethodController = async(req,res)=>{
    const { id } = req.params;
    const deletedPaymentMethod = await getPaymentMethodById(id);
    console.log(deletedPaymentMethod)
    if (deletedPaymentMethod) {
        await deletePaymentMethod(id);
        res.status(200).json(`The method you chose, ${deletedPaymentMethod.payment_method} was deleted.`);  
    } else res.status(400).json("The method you chose was not edited, please check the input entered.");
};

module.exports = { getAllMethodsForAdminController, addingPaymentMethodController, editingPaymentMethodController,
deletingPaymentMethodController };