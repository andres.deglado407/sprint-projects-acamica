const {  getProducts, getProductsForAdmin,addProduct, deleteProduct, changeNameProduct, changePriceProduct, getProductById } = require('../models/product.model');
const clientRedis = require('../redis');

const getProductsController =  async(req,res)=>{
    const products = await getProducts();
    clientRedis.setex('products', 5*60 , JSON.stringify(products));
    res.status(200).json(products);
};
const getProductsForAdminController = async(req,res)=>{
    const products = await getProductsForAdmin();
    const message = {
        message: "Bellow you can see all registered products.",
        products: products
    };
    res.status(200).json(message);
};
const addingProductController =  async(req,res)=>{
    const { product, description, price } = req.body;
    const newProduct = await addProduct(product, description, price);
    if (newProduct[0]) {
        clientRedis.del('products');
        res.status(201).json(`The product ${product}, was succesfully created with the id:  ${newProduct[0]}`);
    } else {
        res.status(400).json("The product was not created, please check your input like foy example the object that you are sending");
    };
};
const editingNameForProductController = async(req,res)=>{
    const { new_name } = req.body;
    const { id } = req.params;
    const changedProduct = await changeNameProduct(id, new_name);
    if (changedProduct[1]==! 0) {
        clientRedis.del('products');
        res.status(201).json(`The product's name was updated into ${new_name}`);
    } else {
        res.status(400).json("The product was not changed, please check your input. Example id or if you are sending correctly the object");
    };
};
const editingPriceForProductController = async(req,res)=>{
    const { new_price } = req.body;
    const { id } = req.params;
    const changedProduct = await changePriceProduct(id, new_price);
    if (changedProduct[1]==! 0) {
        clientRedis.del('products');
        res.status(201).json(`The product's price was updated into ${new_price}`);
    } else {
        res.status(400).json("The product was not changed, please check your input. Example id or if you are sending correctly the object");
    };
};
const deletingProductController =  async(req,res)=>{
    const { id } = req.params;
    const deletedProduct = await getProductById(id);
    if (deletedProduct) {
        await deleteProduct(id);
        clientRedis.del('products');
        res.status(200).json(`The product you chose, ${deletedProduct.product} was deleted.`);
        
    } else {
        res.status(400).json("The product you chose was not deleted, please check the input entered.");
    }
};

module.exports = { getProductsController, getProductsForAdminController, addingProductController,
editingNameForProductController, editingPriceForProductController, deletingProductController };