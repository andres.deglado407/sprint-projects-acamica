const express = require('express');
const router = express.Router();

const { adminValidation } = require('../middlewares/AdminValidation.middleware');
const { addingAddressValidation, deletingAddressValidation } = require('../middlewares/AddressValidation.middleware');
const { userInfoController, getAllUsersController, getAddressesFromUserController,
    addingAddressController, deletingAddressController,inactiveUserController } = require('../controllers/user.controller');



router.get('/', userInfoController);

router.get('/users', adminValidation, getAllUsersController);

router.get('/addresses', getAddressesFromUserController);

router.post('/addresses/add', addingAddressValidation ,addingAddressController);

router.put('/addresses/delete/:id', deletingAddressValidation, deletingAddressController);

router.put('/deactivate_user/:username', adminValidation, inactiveUserController);

module.exports = router; 