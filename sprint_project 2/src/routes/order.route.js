const express = require('express');
const router = express.Router();

const {  orderInputsValidations, orderEditedProductsValidations, orderEditedInputsValidations, 
orderValidatedInDB, orderAndUserValidation, statusValidtationForEditing, orderValidatedByInputsInDB, 
orderValidatedByProductsInDB, userAndAddressValidation  } = require('../middlewares/OrderValidation.middleware'); 
const { adminValidation } = require('../middlewares/AdminValidation.middleware');
const { getAddressesForOrderController, getPaymentMethodsForOrderController, createOrderController, 
    viewOrderController, confirmOrderController , editOrderByProductsController, editOrderByItemsController,
    getUpdateStatusController, updateOrderByAdminController, getUserHistoryOrdersController, getHistoryOrdersController } = require('../controllers/order.controller');


router.get('/create/view_user_addresses', getAddressesForOrderController);

router.get('/create/view_payment_methods', getPaymentMethodsForOrderController);

router.post('/create', orderInputsValidations, orderValidatedInDB, userAndAddressValidation, createOrderController);

router.get('/view/:id', orderAndUserValidation , viewOrderController);

router.put('/confirm/:id', orderAndUserValidation , confirmOrderController);

router.put('/edit_products/:id',orderAndUserValidation, statusValidtationForEditing ,orderEditedProductsValidations, 
orderValidatedByProductsInDB,  editOrderByProductsController);

router.put('/edit_items/:id',orderAndUserValidation, statusValidtationForEditing ,orderEditedInputsValidations, 
orderValidatedByInputsInDB, userAndAddressValidation, editOrderByItemsController);

router.get('/update_status',adminValidation, getUpdateStatusController);

router.put('/update_status/:id', adminValidation, updateOrderByAdminController);

router.get('/history_user',getUserHistoryOrdersController);

router.get('/history_orders', adminValidation ,getHistoryOrdersController );

module.exports = router;