const express = require('express');
const router = express.Router();

const { registrationValidation, emailAndUsernameValidation } = require('../middlewares/RegistrationValidation.middleware');
const { registerController } = require('../controllers/user.controller');

router.post("/",registrationValidation, emailAndUsernameValidation, registerController);      
module.exports = router;

