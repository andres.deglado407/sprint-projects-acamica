const express = require('express');
const router = express.Router();

const { adminValidation } = require('../middlewares/AdminValidation.middleware');
const { addingProductValidation } = require('../middlewares/ProductValidaton.middleware');
const { getProductsController, getProductsForAdminController, addingProductController,
    editingNameForProductController, editingPriceForProductController, 
    deletingProductController } = require('../controllers/product.controller');
const { getCacheProducts } = require('../middlewares/Cache.products') 
    
router.get('/', getCacheProducts, getProductsController);

router.get('/all_products', adminValidation, getProductsForAdminController);

router.post('/add',adminValidation, addingProductValidation, addingProductController);

router.put('/change_name/:id',adminValidation, editingNameForProductController);

router.put('/change_price/:id',adminValidation, editingPriceForProductController);

router.put('/delete/:id',adminValidation, deletingProductController);

module.exports = router;