const express = require('express');
const router = express.Router();


const { addingPaymentMethodValidtation } = require('../middlewares/PaymentMethodValidation.middleware');
const { getAllMethodsForAdminController, addingPaymentMethodController, editingPaymentMethodController,
    deletingPaymentMethodController } = require('../controllers/paymentMethod.controller');

router.get('/', getAllMethodsForAdminController);

router.post('/add', addingPaymentMethodValidtation , addingPaymentMethodController );

router.put('/edit/:id',addingPaymentMethodValidtation, editingPaymentMethodController);

router.put('/delete/:id', deletingPaymentMethodController);

module.exports = router;
