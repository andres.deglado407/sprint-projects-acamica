const express = require('express');
const router = express.Router();



const  { loginInputValidation }  = require('../middlewares/Authenticator.middleware');
const { loginController } = require('../controllers/user.controller');


router.post("/", loginInputValidation ,loginController);

module.exports= router;
