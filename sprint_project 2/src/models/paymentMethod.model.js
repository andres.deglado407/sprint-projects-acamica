const sequelize = require('../config/db');;
const { QueryTypes } = require('sequelize');


const getPaymentMethods = async()=>{
    const paymentMethods = await sequelize.query("SELECT id, payment_method FROM payment_methods WHERE isActive = TRUE",
    {
        type: QueryTypes.SELECT 
    }
    );
    return paymentMethods
};

const getPaymentMethodsForAdmin = async()=>{
    const paymentMethods = await sequelize.query("SELECT * FROM payment_methods",
    {
        type: QueryTypes.SELECT 
    }
    );
    return paymentMethods
};

const getPaymentMethodById = async(id)=>{
    const paymentMethodFound = await sequelize.query("SELECT * FROM payment_methods WHERE id = ? AND isActive = TRUE",
    {
        replacements: [id],
        type: QueryTypes.SELECT
    }
    );
    if (paymentMethodFound){
        return paymentMethodFound[0]
    } else {
        return false 
    }
};

const createPaymentMethod = async(payment_method)=>{
    const addedMethod = await sequelize.query("INSERT INTO payment_methods (payment_method) VALUES(?) ",
    {
        replacements: [payment_method],
            type: QueryTypes.INSERT
    }
    );  
    return addedMethod;
};

const editPaymentMehtod = async(id,editMethod)=>{
    const checkedId = await getPaymentMethods(id);
    if (checkedId) {
        const editedMethod = await sequelize.query("UPDATE payment_methods SET payment_method = ? WHERE id = ?",
        {
            replacements: [editMethod,id],
            type: QueryTypes.UPDATE
        }
        );
        return editedMethod;
    } else {
        return false;
    }
};

const deletePaymentMethod = async(id)=>{
    const checkedId = getPaymentMethods(id);
    if (checkedId) {
        const deletedMethod = await sequelize.query("UPDATE payment_methods SET isActive = FALSE WHERE id = ?",
        {
            replacements: [id],
            type: QueryTypes.UPDATE
        }
        )
        return deletedMethod;
    } else {
         return false;
    }
};

module.exports = { getPaymentMethods, getPaymentMethodsForAdmin, getPaymentMethodById, createPaymentMethod, editPaymentMehtod, deletePaymentMethod};