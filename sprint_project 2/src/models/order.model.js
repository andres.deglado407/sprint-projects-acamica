const sequelize = require('../config/db');
const { QueryTypes } = require('sequelize');

const { findUserByUsername } = require('../models/user.model');
const { getProductById } = require('../models/product.model');

const orderStatus = ["Pendiente","Confirmado","En preparación","Enviado","Entregado"];

const creatingOrderByProducts = async(userlogged, products)=>{
    const newOrder = {
        userClient: userlogged.username,    
        orderStatus: orderStatus[0]
    }
    const t = await sequelize.transaction();
    try {
        const verifiedProducts = await Promise.all(products.map(async(product)=>{
            const verifiedProduct = {
                productInfo: await getProductById(product.id,{transaction:t}),
                quantity: product.quantity
            }
            return verifiedProduct
        }));
        await t.commit();
        newOrder.products = verifiedProducts
        return newOrder;
        
    } catch (error) {
        console.log(error);
        await t.rollback();
    };
};

const sumTotalOfProducts = (order)=>{
    let total = 0;
    order.products.forEach(product => {
        total += product.productInfo.price * product.quantity;
    });
    return total;
};

const addOrderToDB = async(order)=>{
    const userOrder = await findUserByUsername(order.userClient);
    const t = await sequelize.transaction();
    try {
        const orderInDB = await sequelize.query("INSERT INTO orders(order_date, user_id, address_id, payment_method_id, order_status, total) VALUES (NOW(), ?, ?, ?, ?, ?)",
        {
            replacements:[userOrder.id,order.idAddress, order.idPaymentMethod, order.orderStatus, order.total], 
            type: QueryTypes.INSERT
        }
        ,{transaction:t});
        await Promise.all(order.products.map(async(product)=>{
            await sequelize.query("INSERT INTO orders_products(orderId, productId, quantity) VALUES(?, ?,?)",
            {
                replacements:[orderInDB[0], product.productInfo.id, product.quantity]
            }
            ,{transaction: t})
        }));
        await t.commit();
        return orderInDB[0]
    } catch (error) {
        console.log(error);
        await t.rollback();
    };
};

const getOrderByIdFromDB = async(idOrder)=>{
    const t = await sequelize.transaction();
    try {
        const orderInDB = await sequelize.query(`SELECT orders.id, orders.order_date, users.username, addresses.address, payment_methods.payment_method, orders.order_status, orders.total
        FROM orders
        INNER JOIN users
        ON orders.user_id = users.id
        INNER JOIN addresses
        ON orders.address_id = addresses.id
        INNER JOIN payment_methods
        ON orders.payment_method_id = payment_methods.id
        WHERE orders.id = ?`,
        {
            replacements: [idOrder],
            type: QueryTypes.SELECT
        }
        ,{ transaction: t});
        const productsInDB = await sequelize.query(`SELECT products.product, products.price, orders_products.quantity 
        FROM orders_products
        INNER JOIN orders
        ON orders_products.orderId = orders.id
        INNER JOIN products
        ON orders_products.productId = products.id
        WHERE orders.id = ?`,
        {
            replacements: [idOrder],
            type: QueryTypes.SELECT
        }
        ,{ transaction: t});
        await t.commit();
        const orderResult = orderInDB[0];
        orderResult.products = productsInDB;
        return orderResult;    
    } catch (error) {
        console.log(error);
        await t.rollback(); 
    };
};

const editingOrderByProductsInDB = async(orderId, products)=>{
    const t = await sequelize.transaction();
    try {
        const verifiedProducts = await Promise.all(products.map(async(product)=>{
            const verifiedProduct = {
                productInfo: await getProductById(product.id, {transaction:t}),
                quantity: product.quantity
            }
            return verifiedProduct
        }));
        const order = {
            products: verifiedProducts
        }
        const total = sumTotalOfProducts(order)

        await sequelize.query("DELETE FROM orders_products WHERE orderId = ?",
        {
            replacements: [orderId],
            type: QueryTypes.DELETE
        }
        ,{transaction: t});
       
        await Promise.all(verifiedProducts.map(async(product)=>{
            await sequelize.query("INSERT INTO orders_products(orderId, productId, quantity) VALUES(?, ?,?)",
            {
                replacements:[orderId, product.productInfo.id, product.quantity]
            }
            ,{transaction: t})
        }));

        const orderUpdated = await sequelize.query("UPDATE orders SET order_date = NOW(), total = ? WHERE id = ?",
        {
            replacements: [total, orderId],
            type: QueryTypes.UPDATE
        }
        ,{ transaction: t});

        await t.commit();
        return orderUpdated
    } catch (error) {
        console.log(error);
        await t.rollback();
    }
};

const editingOrderByItemsInDB = async(orderId, idAddress, idPaymentMethod)=>{
    const t = await sequelize.transaction();
    try {
        const orderUpdated = await sequelize.query("UPDATE orders SET order_date = NOW(), address_id = ?, payment_method_id = ? WHERE id = ?",
        {
            replacements: [idAddress, idPaymentMethod, orderId],
            type: QueryTypes.UPDATE
        }
        ,{ transaction: t});
        await t.commit();
        return orderUpdated;
    } catch (error) {
        console.log(error);
        await t.rollback();
    }
};

const confirmOrder = async(orderId)=>{
    const orderConfirmed = await sequelize.query("UPDATE orders SET order_status = ? WHERE id = ?",
    {
        replacements: [orderStatus[1],orderId],
        type: QueryTypes.UPDATE
    }
    );
    return orderConfirmed;
};

const editStatusByAdmin =async(idOrder, NumStatus)=>{
    const orderChanged = await sequelize.query("UPDATE orders SET order_status = ? WHERE id = ?",
    {
        replacements: [orderStatus[NumStatus-1],idOrder],
        type: QueryTypes.UPDATE
    }
    );
    return orderChanged;
};

const getOrdersFromUser = async(username)=>{
    const userOrder = await findUserByUsername(username);    
    const t = await sequelize.transaction();
    try {
        const ordersInDB = await sequelize.query(`SELECT orders.id, orders.order_date, users.username, addresses.address, payment_methods.payment_method, orders.order_status, orders.total
        FROM orders
        INNER JOIN users
        ON orders.user_id = users.id
        INNER JOIN addresses
        ON orders.address_id = addresses.id
        INNER JOIN payment_methods
        ON orders.payment_method_id = payment_methods.id
        WHERE orders.user_id = ?`,
        {
            replacements: [userOrder.id],
            type: QueryTypes.SELECT
        }
        ,{ transaction: t});
        const finalOrders = await Promise.all(ordersInDB.map(async(order)=>{
            const productsByOrderInDB = await sequelize.query(`SELECT products.product, products.price, orders_products.quantity 
            FROM orders_products
            INNER JOIN orders
            ON orders_products.orderId = orders.id
            INNER JOIN products
            ON orders_products.productId = products.id
            WHERE orders.id = ?`,
            {
                replacements: [order.id],
                type: QueryTypes.SELECT
            }
            ,{ transaction: t});
            order.products = productsByOrderInDB;
            return order
        }));
        await t.commit();
        return finalOrders;    
    } catch (error) {
        console.log(error);
        await t.rollback(); 
    };
};

const getAllOrders = async()=>{
    const t = await sequelize.transaction();
    try {
        const ordersInDB = await sequelize.query(`SELECT orders.id, orders.order_date, users.username, addresses.address, payment_methods.payment_method, orders.order_status, orders.total
        FROM orders
        INNER JOIN users
        ON orders.user_id = users.id
        INNER JOIN addresses
        ON orders.address_id = addresses.id
        INNER JOIN payment_methods
        ON orders.payment_method_id = payment_methods.id`,
        {
            type: QueryTypes.SELECT
        }
        ,{ transaction: t});
        const finalOrders = await Promise.all(ordersInDB.map(async(order)=>{
            const productsByOrderInDB = await sequelize.query(`SELECT products.product, products.price, orders_products.quantity 
            FROM orders_products
            INNER JOIN orders
            ON orders_products.orderId = orders.id
            INNER JOIN products
            ON orders_products.productId = products.id`,
            {
                type: QueryTypes.SELECT
            }
            ,{ transaction: t});
            order.products = productsByOrderInDB;
            return order
        }));
        await t.commit();
        return finalOrders;    
    } catch (error) {
        console.log(error);
        await t.rollback(); 
    };
};

const getOrderStatus = ()=>{
    return orderStatus;
};



module.exports = { getOrderStatus ,creatingOrderByProducts, 
    sumTotalOfProducts, addOrderToDB, getOrderByIdFromDB,editingOrderByProductsInDB, editingOrderByItemsInDB, editStatusByAdmin,
    confirmOrder, getOrdersFromUser, getAllOrders };
