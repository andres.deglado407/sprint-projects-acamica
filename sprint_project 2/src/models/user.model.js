const sequelize = require('../config/db');;
const { QueryTypes } = require('sequelize');


const getUsers = async()=>{
    const users = await sequelize.query("SELECT * FROM users",
        {
            type: QueryTypes.SELECT
        }
    );
    return users;
};

const findUserByUsername = async(username)=>{
    const userFoundByUsername = await sequelize.query("SELECT * FROM users WHERE username = ?" ,
    {
        replacements: [username],
        type: QueryTypes.SELECT
    }
    );
    return userFoundByUsername[0]
};

const createUser = async(newUser)=>{
    await sequelize.query("INSERT INTO users (username, userpassword, name_, email, tel, isAdmin, isActive) VALUES(?, ?, ?, ?, ?, ?, ?)",
    {
        replacements:[newUser.username, newUser.userpassword ,newUser.name, newUser.email, newUser.tel, newUser.isAdmin, newUser.isActive],
        type: QueryTypes.INSERT
    }
    );
};

const getAddressById = async(id)=>{
    const address = await sequelize.query("SELECT * FROM addresses WHERE id = ?",
    {
        replacements: [id],
        type: QueryTypes.SELECT
    }
    );
    return address[0];
};

const getUserByEmail = async(email)=>{
    const userFoundByEmail = await sequelize.query("SELECT * FROM users WHERE email= ?" ,
    {
        replacements: [email],
        type: QueryTypes.SELECT
    }
    );
    return userFoundByEmail[0]
};

const deactivateUserByUsername = async(username)=>{
    const userToEdit = await findUserByUsername(username);
    if(userToEdit.isActive){
        await sequelize.query("UPDATE users SET isActive = FALSE WHERE username = ?",
        {
            replacements: [username],
            type: QueryTypes.UPDATE
        });
        return true
    } else return false 
};

const getAddressesFromUser = async(user)=>{
    const addresses = await sequelize.query("SELECT id, address FROM addresses WHERE user_id = ? AND isActive = TRUE",
    {
        replacements: [user.id],
        type: QueryTypes.SELECT
    }
    );
    return addresses;
};

const createAddressUser = async(user, address)=>{
        const newAddress = await sequelize.query("INSERT INTO addresses (address, user_id) VALUES(?, ?)",
        {
            replacements: [ address,user.id],
            type: QueryTypes.SELECT
        }
        );
        return newAddress;
    };

const deleteAddressFromUser = async(id, user)=>{
        const deletedAddress = await sequelize.query("UPDATE addresses SET isActive = FALSE WHERE id = ? AND user_id = ?",
        {
            replacements: [id, user.id],
            type: QueryTypes.UPDATE
        }
        );
        return deletedAddress
};

const deleteUserByUsername = async(username)=>{
    const deletedUser = await sequelize.query("DELETE FROM users WHERE username = ?",
    {
        replacements: [username],
        type: QueryTypes.DELETE
    }
    );
    return deletedUser
};



module.exports = {getUsers, findUserByUsername, createUser, getAddressesFromUser, createAddressUser, deleteAddressFromUser, getAddressById, getUserByEmail, deactivateUserByUsername, deleteUserByUsername};

