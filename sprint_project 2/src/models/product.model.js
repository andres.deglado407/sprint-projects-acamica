const sequelize = require('../config/db');;
const { QueryTypes } = require('sequelize');


const getProducts = async()=>{
    const products = await sequelize.query("SELECT id, product, description, price FROM products WHERE isActive = TRUE",
    {
        type: QueryTypes.SELECT
    }
    );
    return products;
};

const getProductsForAdmin = async()=>{
    const products = await sequelize.query("SELECT * FROM products",
    {
        type: QueryTypes.SELECT
    }
    );
    return products;
};
const getProductById = async(id)=>{
    const product = await sequelize.query("SELECT * FROM products WHERE id = ?",
    {
        replacements: [id],
        type: QueryTypes.SELECT
    }
    );
    return product[0];
};

const addProduct = async(product, description, price)=>{

    const newProduct = await sequelize.query("INSERT INTO products (product, description, price) VALUES (?, ?, ?)",
    {
        replacements: [product, description, price],
        type: QueryTypes.INSERT
    }
    );
    return newProduct;
};

const deleteProduct = async(id)=>{
    const foundProduct = await getProductById(id);
    if (foundProduct){
        const deletedProduct = await sequelize.query("UPDATE products SET isActive = FALSE WHERE id = ?",
        {
            replacements: [id],
            type: QueryTypes.UPDATE
        }
        );
        return deletedProduct
    } else return false
};

const changeNameProduct = async(id,newName)=>{
    const foundProduct = await getProductById(id);
    if (foundProduct) {
        if(newName && newName !== "" && newName !== null && (typeof newName === 'string')) {
        const productNewName = await sequelize.query("UPDATE products SET product = ? WHERE id = ?",
        {
            replacements: [newName,id],
            type: QueryTypes.UPDATE
        }
        );
        return productNewName;
        } else return false;
    } else return false;
};

const changePriceProduct = async(id,newPrice)=>{
    const foundProduct = await getProductById(id);
    if (foundProduct) {
        if(newPrice && newPrice !== "" && newPrice !== null && (typeof newPrice === 'number')) {
        const productNewPrice = await sequelize.query("UPDATE products SET price = ? WHERE id = ?",
        {
            replacements: [newPrice,id],
            type: QueryTypes.UPDATE
        }
        );
        return productNewPrice;
        } else return false;
    } else return false;
};


module.exports = { getProducts, getProductsForAdmin, getProductById, addProduct, deleteProduct, changeNameProduct, changePriceProduct };