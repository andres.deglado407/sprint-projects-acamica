const config = require('./config/config');

const redis = require('redis');
const client = redis.createClient(config.REDIS_PORT);

module.exports = client;
