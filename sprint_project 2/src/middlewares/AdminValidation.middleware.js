const { findUserByUsername } = require('../models/user.model');

const adminValidation  = async(req,res,next)=>{
    const userLogged = await findUserByUsername(req.user.username);
    if(userLogged.isAdmin) next();
    else {res.status(403).json('You do not have permission to access this page.')};
};

module.exports = {adminValidation};