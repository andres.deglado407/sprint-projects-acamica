const { QueryTypes } = require('sequelize');

const sequelize = require('../config/db');;
const { findUserByUsername } = require('../models/user.model');
const { UserAddressSchema } = require('../Schemas/UserSchema');


const addingAddressValidation = async(req,res,next)=>{
    try {
        await UserAddressSchema.validateAsync(req.body);
        return next();    
    } catch (error) {
        res.status(400).json(error.details[0].message);
    }
};

const deletingAddressValidation = async(req,res,next)=>{
    const { id } = req.params;
    const userLogged =  await findUserByUsername(req.user.username);
    const address = await sequelize.query("SELECT id, address FROM addresses WHERE user_id = ? and id = ?",
    {
        replacements: [userLogged.id, id],
        type: QueryTypes.SELECT
    }
    );
    if (address[0]) {
        return next();
    } else {
        res.status(403).json('The id you input, does not match with the an address from the user logged.');
    }
};

module.exports = { addingAddressValidation, deletingAddressValidation};
