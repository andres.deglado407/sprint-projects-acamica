const { OrderSchema, EditOrderByItemsSchema, EditOrderByProductsSchema } = require('../Schemas/order.joi');
const { getProductById } = require('../models/product.model');
const { getPaymentMethodById } = require('../models/paymentMethod.model');
const { getAddressById, findUserByUsername } = require('../models/user.model');
const { getOrderByIdFromDB } = require('../models/order.model');

const orderInputsValidations = async(req,res,next) =>{
    try {
        await OrderSchema.validateAsync(req.body);
        return next();
    } catch (error) {
        res.status(400).json(error.details[0].message);
    }
};

const orderEditedProductsValidations = async(req,res,next) =>{
    try {
        await EditOrderByProductsSchema.validateAsync(req.body);
        return next();
    } catch (error) {
        res.status(400).json(error.details[0].message);
    }
};

const orderEditedInputsValidations = async(req,res,next) =>{
    try {
        await EditOrderByItemsSchema.validateAsync(req.body);
        return next();
    } catch (error) {
        res.status(400).json(error.details[0].message);
    }
};

const productsValidation = async(products)=>{
    const verifiedProducts = await Promise.all(products.map(async(product)=>{
        const verifiedProduct = await getProductById(product.id);
        return verifiedProduct
    }));
    const productsNotFound = [];
    const productsInactive = [];
    for (let index = 0; index < verifiedProducts.length; index++) {
        const productFound = verifiedProducts[index];
        const product = products[index];
        if (productFound=== undefined) {
            productsNotFound.push(product.id);
        } else if (productFound.isActive === 0){
            productsInactive.push(product.id);
        }
    };
    const respondProducts = {
        productsNotFound: productsNotFound,
        productsInactive: productsInactive
    }
    return respondProducts;
};

const paymentMethodValidation = async(idPaymentMethod)=>{
    const verifiedPaymentMethod = await getPaymentMethodById(idPaymentMethod);
    if (verifiedPaymentMethod.isActive) {
        return true         
    } else {
        return false
    }
};

const addressdValidation = async(idAddress)=>{
    const verifiedAddress = await getAddressById(idAddress);
    if (verifiedAddress.isActive) {
        return true         
    } else {
        return false
    }
};

const orderValidatedInDB = async(req,res,next)=>{
    const { products, idAddress, idPaymentMethod} = req.body;
    const productsNotValidated =  await productsValidation(products);
    const paymentMethodValidated = await paymentMethodValidation(idPaymentMethod);
    const addressValidated = await addressdValidation(idAddress);
    if (productsNotValidated.productsNotFound.length===0) {
        if (productsNotValidated.productsInactive.length===0) {
            if (addressValidated) {
                if (paymentMethodValidated) {
                    return next();
                } else res.status(404).json("Please check the payment method entered.")
            } else res.status(404).json("Please check your address entered.");           
        } else {
            const respondInactive = {
                message: "The following ids are from products inactive, please watch our catalog of products.",
                id_products: productsNotValidated.productsInactive
            };
            res.status(404).json(respondInactive);
        }
    } else {
        const respondNotFound = {
            message: "The following ids were not found, please watch our catalog of products.",
            id_products: productsNotValidated.productsNotFound
        };
        res.status(404).json(respondNotFound);
    }
};

const orderValidatedByProductsInDB = async(req,res,next)=>{
    const { products } = req.body;
    const productsNotValidated =  await productsValidation(products);
    if (productsNotValidated.productsNotFound.length===0) {
        if (productsNotValidated.productsInactive.length===0) {
            return next();
        } else {
            const respondInactive = {
                message: "The following ids are from products inactive, please watch our catalog of products.",
                id_products: productsNotValidated.productsInactive
            };
            res.status(404).json(respondInactive);
        }
    } else {
        const respondNotFound = {
            message: "The following ids were not found, please watch our catalog of products.",
            id_products: productsNotValidated.productsNotFound
        };
        res.status(404).json(respondNotFound);
    };
};

const orderValidatedByInputsInDB = async(req,res,next)=>{
    const { idAddress, idPaymentMethod} = req.body;
    const paymentMethodValidated = await paymentMethodValidation(idPaymentMethod);
    const addressValidated = await addressdValidation(idAddress);
    if (addressValidated) {
         if (paymentMethodValidated) {
             return next();
        } else res.status(404).json("The id of the paymennt method was not found.");  
    } else  res.status(404).json("The id of the address was not found.");
};

const orderAndUserValidation  = async(req,res,next)=>{
    const { id } = req.params;
    const username = req.user.username;
    const orderInDB = await getOrderByIdFromDB(id);
    if (orderInDB) {
        if (orderInDB.username==username) {
            next();
        } else res.status(400).json("The order selected does not correspond with your user");
    } else res.status(404).json("The id selected does not match with any order");
};

const statusValidtationForEditing = async (req,res,next)=>{
    const { id } = req.params;
    const orderInDB = await getOrderByIdFromDB(id);
    if( orderInDB.order_status === "Pendiente" ){
         next();
    }else res.status(400).json("Your order has been confirmed, therefore it can not be edited");
 

};

const userAndAddressValidation = async(req,res,next)=>{
    const { idAddress } = req.body;
    const userLogged =  await findUserByUsername(req.user.username);
    const addressFound = await getAddressById(idAddress);
    if (addressFound.user_id === userLogged.id) {
        return next();
    } else res.status(403).json("The address entered does not match with on of your addresses");
};

module.exports = { orderInputsValidations, orderEditedProductsValidations, orderEditedInputsValidations, orderValidatedInDB, 
orderAndUserValidation, statusValidtationForEditing, orderValidatedByInputsInDB, orderValidatedByProductsInDB, userAndAddressValidation }; 
