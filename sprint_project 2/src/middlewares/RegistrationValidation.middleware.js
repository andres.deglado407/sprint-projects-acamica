const { getUsers } = require('../models/user.model');
const { UserSchema } = require('../Schemas/UserSchema');


const emailValidation = async(email)=>{
    const users = await getUsers();
    if ( users.find(u => u.email==email)) {
        return false;
    }else return true;   
};

const usernameValidation = async(username)=>{
    const users = await getUsers();
    if (users.find(u => u.username==username)) {
        return false;
    }else return true;   
};

const registrationValidation = async (req,res,next)=>{
    try {
        await UserSchema.validateAsync(req.body);
        return next();
    } catch (error) {
        res.status(400).json(error.details[0].message);
    }
};

const emailAndUsernameValidation = async(req,res,next) =>{
    const email = req.body.email;
    const username = req.body.username;
    if ( await emailValidation(email)) {
        if (await usernameValidation(username)) {
            return next();   
        } else res.status(400).json('This username is already used, please change it.'); 
    } else res.status(400).json('This email is already used, please change it.');      
};
   

module.exports =  { registrationValidation, emailAndUsernameValidation }; 
