const { ProductSchema } = require('../Schemas/product.joi');

const addingProductValidation = async(req,res,next)=>{
    try {
        await ProductSchema.validateAsync(req.body);
        next ();
    } catch (error) {
        res.status(400).json(error.details[0].message);
    }
};

module.exports = { addingProductValidation };