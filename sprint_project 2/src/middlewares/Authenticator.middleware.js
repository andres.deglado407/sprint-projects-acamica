
const { findUserByUsername } = require('../models/user.model');
const { UserLoginSchema } = require('../Schemas/UserSchema')

const loginInputValidation = async(req,res,next)=>{
    try {
        await UserLoginSchema.validateAsync(req.body);
        return next();
    } catch (error) {
        res.status(400).json(error.details[0].message);
    }
};

const isActiveValidation = async(req,res,next) =>{
    const userLogin = await findUserByUsername(req.user.username);
    if (userLogin.isActive) {
        return next();
    } else return res.status(401).json("Your user is inactive, please comunicate with support.")
}

module.exports = { loginInputValidation, isActiveValidation };