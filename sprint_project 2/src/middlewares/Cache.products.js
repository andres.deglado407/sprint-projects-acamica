const clientRedis = require('../redis');

const getCacheProducts = (req,res,next)=>{
    clientRedis.get('products',(err,data)=>{
        if(err) throw err;
        if (data) return res.json(JSON.parse(data));
        else return next();
    });
};

module.exports= { getCacheProducts };