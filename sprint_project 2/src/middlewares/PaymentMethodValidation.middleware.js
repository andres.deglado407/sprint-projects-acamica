const { PaymentMethodSchema } = require('../Schemas/paymentMethod.joi');

const addingPaymentMethodValidtation = async (req,res,next) =>{
    try {
        await PaymentMethodSchema.validateAsync(req.body);
        next();
    } catch (error) {
        res.status(400).json(error.details[0].message);
    }
};

module.exports = {addingPaymentMethodValidtation};