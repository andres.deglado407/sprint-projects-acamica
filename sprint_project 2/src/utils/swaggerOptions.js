const swaggerOptions = {
    definition: 
    {  
        "openapi": "3.0.0",
        "info": {
          "description": "A continuación se muestra la documentación de la API solicitada por el restuaruante Delilah Restó. En esta existen dos tipos de usuarios, el usuario normal y el usuario admin. El usuario normal podrá registrarse a la página y ambos deben ver la lista de pedidos para así conocer los productos ofrecidos. Seguido a esto se procede ir a la ruta de la orden, donde con el id del producto y la cantidad que desean del mismo crean su orden. Por último los usuarios pueden realizar modificaciones hasta que sea confirmado por el mismo. El usuario admin puede contar con acciones adicionales como crear, editar y borrar tanto productos como medios de pago. Espero que disfruten del programa.",
          "version": "2.0.0",
          "title": "Sprint project 2 - Delilah Restó",
          "contact": {
            "email": "andres.deglado407@gmail.com"
          }
        },
        "paths": {
          "/register": {
            "post": {
              "summary": "Registro de usuarios a la API ",
              "description": "se solicita al usuario correo y contraseña. Al ser validado con la base de datos devolvera un token de acceso.",
              "tags": [
                "Usuarios"
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/UsuarioRegistrado"
                    }
                  }
                }
              },
              "responses": {
                "201": {
                  "description": "Confirma la creación del usuario en la base de datos, con la información suministrada",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "User created successfully"
                      }
                    }
                  }
                },
                "400": {
                  "description": "Entradas no son validas, puede ser por usar un email o usuario ya registrado, como también la falta de uno de los campos."
                }
              },
              "security": []
            }
          },
          "/login": {
            "post": {
              "summary": "Login de usario",
              "description": "El usuario ya registrado podrá ingresar a la API con el correo y contraseña anteriormente suministradas",
              "tags": [
                "Usuarios"
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/UsuarioLogueado"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Devuelve un mensaje de éxito con el token generado"
                },
                "400": {
                  "description": "Entradas no son validas"
                },
                "401": {
                  "description": "Cuando no coincide la contraseña o el correo con los registrados, como también que el usuario se encuentre desactivado por el admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "Unauthorized"
                      }
                    }
                  }
                },
                "404": {
                  "description": "Cuando el usuario loguea con correo y contraseña no registrados.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "There is no registred user with that email. Please register first."
                      }
                    }
                  }
                }
              },
              "security": []
            }
          },
          "/user_info": {
            "get": {
              "summary": "Muestra la información del usuario logueado",
              "description": "Con el token suministrado, el usuario visualiza información de él ingresada en el registro",
              "tags": [
                "Usuarios"
              ],
              "responses": {
                "200": {
                  "description": "Crea un usuario en el array de usuarios",
                  "content": {
                    "application/json": {
                      "schema": {
                        "$ref": "#/components/schemas/InfoUsuario"
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                }
              }
            }
          },
          "/user_info/users": {
            "get": {
              "summary": "Usuarios registrados",
              "description": "Con en el login del usuario admin, este podrá ver a todos los usuarios registrados, tanto activos como inactivos.",
              "tags": [
                "Usuarios"
              ],
              "responses": {
                "200": {
                  "description": "Crea un usuario en el array de usuarios",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/UsuariosInfo"
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/user_info/addresses": {
            "get": {
              "summary": "Direcciones de envío del usuario logueado",
              "description": "En caso de haber registrado direcciones, en este endpoint se retorna la agenda de direcciones del usuario logueado.",
              "tags": [
                "Usuarios"
              ],
              "responses": {
                "200": {
                  "description": "Array de direcciones del usuario",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/DireccionesDeUsuario"
                        }
                      }
                    }
                  }
                },
                "400": {
                  "description": "Respuesta cuando un usuario tiene ninguna dirección registrada",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "Please enter first an address."
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                }
              }
            }
          },
          "/user_info/addresses/add": {
            "post": {
              "summary": "Registro de dirección de envío en la API ",
              "description": "En este apartado el usuario logueado ingresa direcciones de envío en su agenda.",
              "tags": [
                "Usuarios"
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/AgregarDireccion"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Confirma el registro de la dirección con el número de id, para ser seleccionado en otro endpoint."
                },
                "400": {
                  "description": "Entradas no son validas debido al cuerpo que se está enviando."
                }
              }
            }
          },
          "/user_info/addresses/delete/{id}": {
            "put": {
              "summary": "Desactivacion de la dirección ",
              "description": "En este apartado el usuario logueado desactiva la dirección con el id generado.",
              "tags": [
                "Usuarios"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id de la dirección de envío"
                }
              ],
              "responses": {
                "200": {
                  "description": "Confirma la desactivación de la dirección, de manera que esta no podrá ser seleccionada para los envíos.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You have deleted the address successfully."
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "La entrada, es decir el ID de la dirección, no corresponde al usuario logueado.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "The id you input, does not match with the an address from the user logged."
                      }
                    }
                  }
                }
              }
            }
          },
          "/user_info/deactivate_user/{username}": {
            "put": {
              "summary": "Desactivacion del usuario ",
              "description": "Con el login del usuario admin, en este apartado a través del username de un usuario, puede desactivar a dicho usuario. Esto implicaría que el usuario no podrá generar token y en caso de hacerlo, de igual forma no podrá ingresar a los endpoints",
              "tags": [
                "Usuarios"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "username",
                  "schema": {
                    "type": "string"
                  },
                  "required": true,
                  "description": "username del usuario a desactivar"
                }
              ],
              "responses": {
                "200": {
                  "description": "Confirma la desactivación del usuario seleccionado, de manera que éste no podrá ingresar a la página y no se le generará un token de acceso."
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                },
                "404": {
                  "description": "El parametro de usuario no fue encontrado dentro de los usuarios registrados.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "The username entered was not found."
                      }
                    }
                  }
                }
              }
            }
          },
          "/payment_methods": {
            "get": {
              "summary": "Todos los medios de pago registrados",
              "description": "Con en el login del usuario admin, este podrá ver a todos los medios de pago registrados en el sistema, tanto activo como inactivo.",
              "tags": [
                "Métodos de pago"
              ],
              "responses": {
                "200": {
                  "description": "Array con todos los méotdos de pago",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/MetodosDePago"
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/payment_methods/add": {
            "post": {
              "summary": "Ingreso de nuevo método de pago",
              "description": "Con en el login del usuario admin, este podrá ingresar nuevos medios de pago.",
              "tags": [
                "Métodos de pago"
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/AgregarMetodoDePago"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Confirma la creación del método de pago."
                },
                "400": {
                  "description": "Respuesta a entradas no válidas en el cuerpo"
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/payment_methods/edit/{id}": {
            "put": {
              "summary": "Editar el medio de pago",
              "description": "Con en el login del usuario admin, este podrá editar el nombre del medio de pago a través del id del mismo.",
              "tags": [
                "Métodos de pago"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id del método de pago"
                }
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/AgregarMetodoDePago"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Confirma la edición del método de pago."
                },
                "400": {
                  "description": "Respuesta a entradas no válidas en el cuerpo"
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/payment_methods/delete/{id}": {
            "put": {
              "summary": "Desactivar un  medio de pago",
              "description": "Con en el login del usuario admin, este podrá desactivar los métodos de pago de manera que ya no estará disponible para los otros usuarios para su seleccion.",
              "tags": [
                "Métodos de pago"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id del método de pago"
                }
              ],
              "responses": {
                "200": {
                  "description": "Confirma la desactivación del método de pago."
                },
                "400": {
                  "description": "Respuesta a entradas no válidas en el cuerpo"
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/products": {
            "get": {
              "summary": "Devuelve un array de productos",
              "description": "El usuario logueado podrá visualizar los productos activos para ser seleccionados por id y ser agregados a la orden.",
              "tags": [
                "Productos"
              ],
              "responses": {
                "200": {
                  "description": "Array con todos productos",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/Productos"
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                }
              }
            }
          },
          "/products/all_products": {
            "get": {
              "summary": "Devuelve un array de todos los productos registrados",
              "description": "Con en el registro del usuario admin, este podrá ver a todos los products registrados en el sistema, tanto activo como inactivo.",
              "tags": [
                "Productos"
              ],
              "responses": {
                "200": {
                  "description": "Array con todos los méotdos de pago",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/ProductosAdmin"
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/products/add": {
            "post": {
              "summary": "Ingreso de nuevo producto",
              "description": "Con en el registro del usuario admin, este podrá ingresar nuevos productos.",
              "tags": [
                "Productos"
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/AgregarProducto"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Confirma la creación del Producto."
                },
                "400": {
                  "description": "Respuesta a entradas no válidas en el cuerpo"
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/products/change_name/{id}": {
            "put": {
              "summary": "Editar el nombre del producto",
              "description": "Con en el registro del usuario admin, este podrá editar el nombre del producto a través del id del mismo.",
              "tags": [
                "Productos"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id del producto"
                }
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/EditarNombreProducto"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Confirma la edición del Producto."
                },
                "400": {
                  "description": "Respuesta a entradas no válidas en el cuerpo"
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/products/change_price/{id}": {
            "put": {
              "summary": "Editar el precio del producto",
              "description": "Con en el registro del usuario admin, este podrá editar el precio del producto a través del id del mismo.",
              "tags": [
                "Productos"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id del Producto"
                }
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/EditarPrecioProducto"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Confirma la edición del producto."
                },
                "400": {
                  "description": "Respuesta a entradas no válidas en el cuerpo"
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/products/delete/{id}": {
            "put": {
              "summary": "Desactivar un producto",
              "description": "Con en el login del usuario admin, este podrá desactivar un producto de manera que ya no estará disponible para los otros usuarios para su seleccion.",
              "tags": [
                "Productos"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id del producto"
                }
              ],
              "responses": {
                "200": {
                  "description": "Confirma la desactivación del producto."
                },
                "400": {
                  "description": "Respuesta a entradas no válidas en el cuerpo"
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/order/create/view_user_addresses": {
            "get": {
              "summary": "Devuelve la agenda de direcciones del usuario",
              "description": "El usuario logueado podrá visualizar su agenda de direcciones con el fin de seleccionar a través del Id para la creación del orden.",
              "tags": [
                "Ordenes"
              ],
              "responses": {
                "200": {
                  "description": "Array con todas las direcciones del usuario",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/DireccionesDeUsuario"
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "404": {
                  "description": "En caso tal el usuario logueado no tiene direcciones registradas, se le solicita primero agregar al menos una dirección.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "Please enter an address first."
                      }
                    }
                  }
                }
              }
            }
          },
          "/order/create/view_payment_methods": {
            "get": {
              "summary": "Devuelve los medios de pago",
              "description": "El usuario logueado podrá visualizar los medios de pago habilitaos para la selección de estos en la creación de la orden.",
              "tags": [
                "Ordenes"
              ],
              "responses": {
                "200": {
                  "description": "Array con todas los medios de pago habilitados",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/MetodosDePagoUsuario"
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                }
              }
            }
          },
          "/order/create": {
            "post": {
              "summary": "Creación de la orden",
              "description": "El usuario logueado podrá enviar los porductos con los que desea crear su orden, con los Ids y cantidad de cada uno, como también la dirección del envío y el método de pago, con el Id de cada uno.",
              "tags": [
                "Ordenes"
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/CrearOrden"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Confirma la creación de la orden, con el retorno de la orden, el usuario quien lo pidió, total y el id de la orden para realizar posibles modificaciones."
                },
                "400": {
                  "description": "Respuesta a entradas no válidas en el cuerpo, falta de id, cantidad, etc."
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "invalid token..."
                      }
                    }
                  }
                },
                "403": {
                  "description": "Respuesta cuando el usuario logueado ingresa un id de dirección no correspondiente a los de su agenda."
                },
                "404": {
                  "description": "Respuesta cuando el usuario logueado ingresa un id de porducto o medio de pago no habilitado o registrado en la base de datos."
                }
              }
            }
          },
          "/order/view/{id}": {
            "get": {
              "summary": "Visualizar una orden por Id",
              "description": "El usuario logueado podrá visualizar una orden ya creada a través de su Id.",
              "tags": [
                "Ordenes"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id de la orden"
                }
              ],
              "responses": {
                "200": {
                  "description": "Retorna la orden con propiedades importantes tales como id, nombre del usuario, los productos, cantidades, dirección, medio de pago y total",
                  "content": {
                    "application/json": {
                      "schema": {
                        "$ref": "#/components/schemas/Orden"
                      }
                    }
                  }
                },
                "400": {
                  "description": "En caso tal la orden no corresponde al usuario que está buscando la orden.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "The order selected does not correspond with your user."
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido. Esta respuesta también se puede dar por el ingreso de un usuario dado de baja por el admin."
                },
                "404": {
                  "description": "Cuando se ingresa un Id que no se encuentra registrado en la base de datos|.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "The id selected does not match with any order."
                      }
                    }
                  }
                }
              }
            }
          },
          "/order/confirm/{id}": {
            "put": {
              "summary": "Confirmar la orden",
              "description": "El usuario logueado podrá confirmar una orden ya creada a través del Id.",
              "tags": [
                "Ordenes"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id de la orden"
                }
              ],
              "responses": {
                "200": {
                  "description": "Confirma la creación .",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "Your order has been confirmed."
                      }
                    }
                  }
                },
                "400": {
                  "description": "En caso tal la orden no corresponde al usuario que está buscando la orden.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "The order selected does not correspond with your user."
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido. Esta respuesta también se puede dar por el ingreso de un usuario dado de baja por el admin."
                },
                "404": {
                  "description": "Cuando se ingresa un Id que no se encuentra registrado en la base de datos.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "The id selected does not match with any order."
                      }
                    }
                  }
                }
              }
            }
          },
          "/order/edit_products/{id}": {
            "put": {
              "summary": "Editar la orden, cambiando los productos",
              "description": "El usuario logueado podrá editar la orden enviando una nueva lista de productos con sus respectivos Ids y cantiades. Para esto se tiene que enviar el id de la orden",
              "tags": [
                "Ordenes"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id de la orden"
                }
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/EditarProductosDeOrden"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Confirma la edición del producto devolviendo la orden ya editada."
                },
                "400": {
                  "description": "En caso tal la orden no corresponde al usuario que está buscando la orden.También este estado acude a cuando la orden ya fue confirmada y por lo tanto ya no se puede editar. Por último también este se usa cuando el cuerpo de edición no corresponde con las propiedades que se piden."
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido. Esta respuesta también se puede dar por el ingreso de un usuario dado de baja por el admin."
                },
                "404": {
                  "description": "Cuando se ingresa un Id que no se encuentra registrado en la base de datos.En este caso sería por un Id que no corresponde a algún producto."
                }
              }
            }
          },
          "/order/edit_items/{id}": {
            "put": {
              "summary": "Editar la orden, cambiando medio de pago y dirección de envío",
              "description": "El usuario logueado podrá editar la orden enviando los Ids del medio de pago y dirección de envío. Para esto se tiene que enviar el id de la orden",
              "tags": [
                "Ordenes"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id de la orden"
                }
              ],
              "requestBody": {
                "content": {
                  "application/json": {
                    "schema": {
                      "$ref": "#/components/schemas/EditarItemsDeOrden"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "Confirma la edición del producto devolviendo la orden ya editada."
                },
                "400": {
                  "description": "En caso tal la orden no corresponde al usuario que está buscando la orden.También este estado acude a cuando la orden ya fue confirmada y por lo tanto ya no se puede editar. Por último también este se usa cuando el cuerpo de edición no corresponde con las propiedades que se piden."
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido. Esta respuesta también se puede dar por el ingreso de un usuario dado de baja por el admin."
                },
                "404": {
                  "description": "Cuando se ingresa un Id que no se encuentra registrado en la base de datos.En este caso sería por un Id que no corresponde a alguna dirección o medio de pago."
                }
              }
            }
          },
          "/order/update_status": {
            "get": {
              "summary": "Los estados los que pueden tomar las ordenes",
              "description": "Con en el login del usuario admin, en este endpoint podrá ver los códigos de los estados que pueden tomar las ordenes, para de esta manera poder editarlos.",
              "tags": [
                "Ordenes"
              ],
              "responses": {
                "200": {
                  "description": "Muestra los códigos de los estados de orden para ser usado en el siguiente endpoint."
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido. Esta respuesta también se puede dar por el ingreso de un usuario dado de baja por el admin."
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/order/update_status/{id}": {
            "put": {
              "summary": "Los estados los que pueden tomar las ordenes",
              "description": "Con en el login del usuario admin, en este endpoint podrá ver los códigos de los estados que pueden tomar las ordenes, para de esta manera poder editarlos.",
              "tags": [
                "Ordenes"
              ],
              "parameters": [
                {
                  "in": "path",
                  "name": "id",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "Id de la orden"
                },
                {
                  "in": "query",
                  "name": "update_status",
                  "schema": {
                    "type": "integer"
                  },
                  "required": true,
                  "description": "código del estado"
                }
              ],
              "responses": {
                "200": {
                  "description": "Muestra los códigos de los estados de orden para ser usado en el siguiente endpoint."
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido. Esta respuesta también se puede dar por el ingreso de un usuario dado de baja por el admin."
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                },
                "404": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          },
          "/order/history_user": {
            "get": {
              "summary": "Historial de usuario",
              "description": "El usuario logueado podrá visualizar su historial de ordenes.",
              "tags": [
                "Ordenes"
              ],
              "responses": {
                "200": {
                  "description": "Se visualiza en un array todas las ordenes realizadas por el usuario logueado.",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/Orden"
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido. Esta respuesta también se puede dar por el ingreso de un usuario dado de baja por el admin."
                }
              }
            }
          },
          "/order/history_orders": {
            "get": {
              "summary": "Historial de ordenes en la API",
              "description": "Con en el login del usuario admin, en este endpoint podrá ver todo el historial de ordenes en la API.",
              "tags": [
                "Ordenes"
              ],
              "responses": {
                "200": {
                  "description": "Se visualiza en un array todas las ordenes.",
                  "content": {
                    "application/json": {
                      "schema": {
                        "type": "array",
                        "items": {
                          "$ref": "#/components/schemas/Orden"
                        }
                      }
                    }
                  }
                },
                "401": {
                  "description": "En caso tal se suministre un token invalido. Esta respuesta también se puede dar por el ingreso de un usuario dado de baja por el admin."
                },
                "403": {
                  "description": "Respuesta cuando un usuario que no es admin ingresa a un endpoint de solo admin.",
                  "content": {
                    "text/plain": {
                      "schema": {
                        "type": "string",
                        "example": "You do not have permission to access this page."
                      }
                    }
                  }
                }
              }
            }
          }
        },
        "security": [
          {
            "bearerAuth": []
          }
        ],
        "components": {
          "securitySchemes": {
            "bearerAuth": {
              "type": "http",
              "scheme": "bearer",
              "bearerFormat": "JWT"
            }
          },
          "schemas": {
            "UsuarioRegistrado": {
              "type": "object",
              "required": [
                "username",
                "userpassword",
                "repeatpassword",
                "name",
                "email",
                "tel"
              ],
              "properties": {
                "username": {
                  "type": "string"
                },
                "userpassword": {
                  "type": "string"
                },
                "repeatpassword": {
                  "type": "string"
                },
                "name": {
                  "type": "string"
                },
                "email": {
                  "type": "string"
                },
                "tel": {
                  "type": "string"
                }
              },
              "example": {
                "username": "UserTest",
                "userpassword": "12345",
                "repeatpassword": "12345",
                "name": "User Test From The API",
                "email": "usertest@gmai.com",
                "tel": "3137524810"
              }
            },
            "UsuarioLogueado": {
              "type": "object",
              "required": [
                "email",
                "userppassword"
              ],
              "properties": {
                "email": {
                  "type": "string"
                },
                "userpassword": {
                  "type": "string"
                }
              },
              "example": {
                "email": "usertest@gmai.com",
                "userpassword": "12345"
              }
            },
            "InfoUsuario": {
              "type": "object",
              "properties": {
                "username": {
                  "type": "string"
                },
                "name_": {
                  "type": "string"
                },
                "email": {
                  "type": "string"
                },
                "tel": {
                  "type": "string"
                }
              },
              "example": {
                "username": "UserTest",
                "name_": "User Test From The API",
                "email": "usertest@gmai.com",
                "tel": "3137524810"
              }
            },
            "UsuariosInfo": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "username": {
                  "type": "string"
                },
                "userpassword": {
                  "type": "string"
                },
                "name_": {
                  "type": "string"
                },
                "email": {
                  "type": "string"
                },
                "tel": {
                  "type": "string"
                },
                "isAdmin": {
                  "type": "boolean"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "example": {
                "id": 2,
                "username": "UserTest",
                "userpassword": "$10$DQ3NiKsC5qBmVhRVL/HzteJvOv9VhPHUBVNVW2E5.1638esTttGYC",
                "name_": "User numero uno",
                "email": "user1@gmai.com",
                "tel": "3512851469",
                "isAdmin": false,
                "isActive": false
              }
            },
            "DireccionesDeUsuario": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "address": {
                  "type": "string"
                }
              },
              "example": {
                "id": 1,
                "address": " cra 84a no 15-05 cali-col",
              }
            },
            "AgregarDireccion": {
              "type": "object",
              "required": [
                "address"
              ],
              "example": {
                "address": " cra 84a no 15-05 cali-col"
              }
            },
            "MetodosDePago": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "payment_method": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "example": {
                "id": 1,
                "payment_method": "Efectivo",
                "isActive": true
              }
            },
            "AgregarMetodoDePago": {
              "type": "object",
              "properties": {
                "payment_method": {
                  "type": "string"
                }
              },
              "required": [
                "payment_method"
              ],
              "example": {
                "payment_method": "Pay Pal"
              }
            },
            "Productos": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "product": {
                  "type": "string"
                },
                "description": {
                  "type": "string"
                },
                "price": {
                  "type": "number"
                }
              },
              "example": {
                "id": 1,
                "product": "Sandwich",
                "description": "emparedado de jamón y queso",
                "price": 7500
              }
            },
            "ProductosAdmin": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "product": {
                  "type": "string"
                },
                "description": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "example": {
                "id": 1,
                "product": "Sandwich",
                "description": "emparedado de jamón y queso",
                "price": 7500,
                "isActive": true
              }
            },
            "AgregarProducto": {
              "type": "object",
              "properties": {
                "product": {
                  "type": "string"
                },
                "description": {
                  "type": "string"
                },
                "price": {
                  "type": "number"
                }
              },
              "required": [
                "product",
                "description",
                "price"
              ],
              "example": {
                "product": "Empanadas para compartir",
                "description": "10 empanadas de carne fritas",
                "price": 7800
              }
            },
            "EditarNombreProducto": {
              "type": "object",
              "properties": {
                "new_name": {
                  "type": "string"
                }
              },
              "required": [
                "new_name"
              ],
              "example": {
                "new_name": "Combo empanadas"
              }
            },
            "EditarPrecioProducto": {
              "type": "object",
              "properties": {
                "new_price": {
                  "type": "number"
                }
              },
              "required": [
                "new_price"
              ],
              "example": {
                "new_price": 10000
              }
            },
            "MetodosDePagoUsuario": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "payment_method": {
                  "type": "string"
                }
              },
              "example": {
                "id": 1,
                "payment_method": "Efectivo"
              }
            },
            "CrearOrden": {
              "type": "object",
              "required": [
                "products",
                "idAddress",
                "idPaymentMethod"
              ],
              "properties": {
                "products": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "integer"
                      },
                      "quantity": {
                        "type": "integer"
                      }
                    }
                  }
                },
                "idAddress": {
                  "type": "integer"
                },
                "idPaymentMethod": {
                  "type": "integer"
                }
              },
              "example": {
                "products": [
                  {
                    "id": 1,
                    "quantity": 2
                  },
                  {
                    "id": 2,
                    "quantity": 5
                  }
                ],
                "idAddress": 2,
                "idPaymentMethod": 1
              }
            },
            "Orden": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer"
                },
                "order_date": {
                  "type": "string"
                },
                "username": {
                  "type": "string"
                },
                "address": {
                  "type": "string"
                },
                "payment_method": {
                  "type": "string"
                },
                "order_status": {
                  "type": "string"
                },
                "total": {
                  "type": "number"
                },
                "products": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "product": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "price": {
                        "type": "number"
                      }
                    }
                  }
                }
              },
              "example": {
                "id": 2,
                "order_date": "2021-09-27T18:06:07.000Z",
                "username": "useradmin",
                "address": "Cra 84a No 15-29",
                "payment_method": "Efectivo",
                "order_status": "Pendiente",
                "total": "43000,",
                "products": [
                  {
                    "product": "Pizza personal",
                    "price": 25000,
                    "quantity": 1
                  },
                  {
                    "product": "Pizza pantalon",
                    "price": 18000,
                    "quantity": 1
                  }
                ]
              }
            },
            "EditarProductosDeOrden": {
              "type": "object",
              "required": [
                "products"
              ],
              "properties": {
                "products": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "integer"
                      },
                      "quantity": {
                        "type": "integer"
                      }
                    }
                  }
                }
              },
              "example": {
                "products": [
                  {
                    "id": 1,
                    "quantity": 1
                  },
                  {
                    "id": 2,
                    "quantity": 1
                  }
                ]
              }
            },
            "EditarItemsDeOrden": {
              "type": "object",
              "required": [
                "idAddress",
                "idPaymentMethod"
              ],
              "properties": {
                "idAddress": {
                  "type": "integer"
                },
                "idPaymentMethod": {
                  "type": "integer"
                }
              },
              "example": {
                "idAddress": 2,
                "idPaymentMethod": 1
              }
            }
          }
        },
        "servers": [
          {
            "url": "http://localhost:3000",
            "description": "Local Server"
          }
        ]
      
    },
    apis: [ ]
};

module.exports= swaggerOptions;